"""
    Ova datoteka pokreće selenium webdriver i ide po zadanim danima, preuzima linkove te sprema linkove u tekstualnu datoteku. Pokreće se kao i svaka python datoteka.
"""
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time
import datetime

# Za brisanje prilikom svakog pokretanja
# with open('linkovi.txt', 'w') as f:
#     f.write("")

# Postavljanje webdrivera od Firefoxa i profila za Firefox da ne učitava slike i flash playera
firefox_profile = webdriver.FirefoxProfile()
firefox_profile.set_preference('permissions.default.stylesheet', 2)
firefox_profile.set_preference('permissions.default.image', 2)
firefox_profile.set_preference(
    'dom.ipc.plugins.enabled.libflashplayer.so', 'false')

# Definiranje webdrivera sa prethodno postavljenim profilom
driver = webdriver.Firefox(firefox_profile=firefox_profile)
driver.set_window_size(1200, 600)
# Odlazak na stranicu
driver.get("http://hr.n1info.com/Najnovije")

# Dobivanje putanje do gumba za Cookies jer radi probleme, pritisak na gumb i čekanje od 3 sekunde
xpath_cookies = "//a[contains(@class, 'CybotCookiebotDialogBodyButton')]"
driver.find_element_by_xpath(xpath_cookies).click()
time.sleep(1)

# xPath do gumba za otvaranje kalendara
xpath_gumb_kal = "//div[@class='date-wrapper']"
# xPath do gumba za odabir datuma
xpath_gumb_kal_odabir = "//div[@class='pika-lendar']/table[@class='pika-table']/tbody/tr[@class='pika-row']/td[contains(@class, '') or contains(@class, 'is-today')]/button"
# xPath do guma za učitavanje ostalih članaka
xpath_ucitaj_jos = "//div[@class='load-more-btn-wrapper']/a[contains(@class, 'load-more-btn') and contains(@class, 'js-btn-load-more')]"
# xPath za sakupljanje svih linkova za scrapanje članaka
xpath_linkovi = "//section[@id='filter']/div[contains(@class, 'container') and contains(@class, 'news-container')]/div[contains(@class, 'news-content-list') and contains(@class, 'js-filter-search-container')]/article/div[@class='text-wrapper']/h2[@class='title']/a"


dan = 1
mjesec = 1
links = []
dan_kraj = 30
mjesec_kraj = 11

# Algoritam radi sve dok ne dođe do datuma za kraj
while not (dan == dan_kraj+1 and mjesec == mjesec_kraj):
    # time.sleep(3)
    try:
        # Provjera postoji li navedeni datum (jednostavno rješenje za promijenu mjeseca, odnosno prijelazak na sljedeći mjesec)
        newDate = datetime.datetime(2020, mjesec, dan)
        print(newDate)
        # Pritisak na gumb za otvaranje kalendara
        driver.find_element_by_xpath(xpath_gumb_kal).click()
        # Traženje elementa za odabir datuma
        datum = driver.find_element_by_xpath(xpath_gumb_kal_odabir)
        # Postavljanje atributa na gumb (dan i mjesec). Promjenom tih atributa lako mijenjamo datum na stranici
        driver.execute_script(
            "arguments[0].setAttribute('data-pika-month', "+str(mjesec-1)+")", datum)
        driver.execute_script(
            "arguments[0].setAttribute('data-pika-day',"+str(dan)+")", datum)

        # Pritisak na gumb sa podešenim datumom
        datum.click()

        timeout_counter = 0
        # Pritiskanje na gumb "Učitaj još" sve dok postoji
        while (True):
            """ try:
                 driver.find_element_by_xpath(xpath_ucitaj_jos).click()
             finally:
                 break"""
            try:
                #element = driver.find_element_by_xpath(xpath_ucitaj_jos)
                time.sleep(1)
                element = WebDriverWait(driver, 1).until(
                    EC.element_to_be_clickable(
                        (By.XPATH, xpath_ucitaj_jos))
                )

                element.click()

                #print("Novi linkovi")
            except:
                timeout_counter += 1
                time.sleep(.5)
                #print("Nije pronađen gumb")
                if timeout_counter == 3:
                    timeout_counter = 0
                    break

        # Prolazak kroz sve nađene elemente i spremanje u varijablu links
        driver.implicitly_wait(35)

        elems = driver.find_elements_by_xpath(xpath_linkovi)

        links = [elem.get_attribute('href') for elem in elems]

        with open('linkovi.txt', 'a') as f:
            for link in links:
                f.write("%s\n" % link)

        # Zapisivanje datuma do kojeg je stiglo (nakon nekog vremena se program malo pogubi zbog krivog učitavanja stranice)
        with open('datum.txt', 'w') as f:
            f.write(str(newDate))

        dan += 1

    # Ukoliko nije važeći datum, povećavamo mjesec za jedan i dan postavljamo na 1
    except ValueError:
        mjesec += 1
        dan = 1

# Završetak, gašenje drivera
driver.quit()

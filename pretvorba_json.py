import csv
import json

data = {}
data['podaci_json'] = []
header = True

# Otvaranje preuzetih podataka i prolazak kroz sve te spremanje u data varijablu
with open("scrapanje_novosti/podaci_final.csv", "r", encoding='utf-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        if header:
            header = False
        else:
            data['podaci_json'].append({'id': lines[0].strip(),
                                        'url': lines[1].strip(),
                                        'naslov': lines[2].strip(),
                                        'kategorija': lines[3].strip(),
                                        'autor': lines[4].strip(),
                                        'datum': lines[5].strip(),
                                        'uvod': lines[6].strip(),
                                        'text': lines[7].strip(),
                                        'br_komentara': lines[8].strip(),
                                        'tagovi': lines[9].strip()})


# Spremanje u JSON datoteku

json_string = json.dumps(data, indent=4, ensure_ascii=False).encode('utf8')

with open("podaci_final.json", 'w', encoding='utf-8') as json_file:
    json_file.write(json_string.decode())

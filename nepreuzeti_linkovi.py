"""
    Ova datoteka provjerava koji linkovi su preuzeti koji nisu.
"""
import csv

# Stvaranje praznih lista
linkovi_zavrseni = []
linkovi_svi = []
found = False

# Otvaranje popisa linkova
with open('../linkovi.txt', 'r') as f:
    linkovi_svi = f.readlines()

# Otvaranje preuzetih podataka i preuzimanje prvog stupca (stupac sa linkovima)
with open("./podaci.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        linkovi_zavrseni.append(lines[0])


# Varijable za ispis trenutnog linka koji se provjerava
counter = 1
maximum = len(linkovi_svi)

# Algoritam koji uzima sve linkove jedan po jedan te provjerava sa linkovima preuzetim u .csv datoteci. Ukoliko pronađe link (odnosno link je preuzet) preskače na sljedeći. Ako ne pronađe link onda taj link sprema u datoteku nepreuzetih linkova
# Ovaj program je koristan jer je stranica blokirala scrapy nakon nekoliko zahtjeva.
with open('../linkovi_nepreuzeti.txt', 'a') as f:
    for link in linkovi_svi:
        for l in linkovi_zavrseni:
            if link.strip() in l.strip():
                found = True
                break
            else:
                found = False
        if not found:
            f.write("%s" % link)
        else:
            found = False
        print(counter, "/", maximum)
        counter += 1

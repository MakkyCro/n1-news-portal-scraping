from scrapy.spiders import Spider
from vijest import Vijest


class NovostiSpider(Spider):
    name = 'novosti'

    # Stvaranje nove csv datoteke
    """f = open('podaci.csv', 'w')
    f.close()"""

    # Učitavanje linkova
    with open("../nepreuzeti_clanci.txt") as f:
        urls = [url.strip() for url in f.readlines()]
        start_urls = urls
        # print(start_urls)

    def parse(self, response):
        # xPath do kategorije
        """xpath_kategorija = "//article/div[@class='single-article-header']/aside[contains(@class, 'info-and-social-header') and contains(@class,'info')]/div[@class='article-info-cnt']/p[@class='category-name']/a[@class='theme-text-color']/text()"

        # xPath do naslova
        xpath_naslov = "//article/div[@class='single-article-header']/h1[@class='single-article-title']/text()"

        # xPath do autora
        xpath_autor = "//div[@class='author']/div[@class='author-text']/div[@class='author-name']/a/text()"

        # xPath do datuma
        xpath_datum = "//div[@class='article-info-cnt']/div[@class='time-and-comment-wrapper']/time[@class='pub-date']/span[@class='date']/text()"

        # xPath do broja komentara
        xpath_br_komentara = "//div[@class='article-info-cnt']/div[@class='time-and-comment-wrapper']/div[@class='comment-number-wrapper']/a[contains(@class, 'js-number-of-comments') and contains(@class,'number-of-comments')]/span[@class='comment-number']/text()"

        # xPath do uvoda
        xpath_uvod = "//div[@id='articleContent']/p[@class='story-lead']/text()"

        # xPath do texta
        xpath_text = "//div[@id='articleContent']/p[not(@class)]/text()"

        # xPath do tagova
        xpath_tagovi = "//div[@class='tags-content']/div[@class='tags']/a[@class='primary-tag']/text()"

        # Dobivanje naslova
        naslov = response.xpath(xpath_naslov).get()

        # Dobivanje uvodnog teksta
        uvod = response.xpath(xpath_uvod).get()

        # Dobivanje kategorije
        kategorija = response.xpath(xpath_kategorija).get()

        # Dobivnje datuma
        datum = response.xpath(xpath_datum).get(
        )

        # Dobivanje autora
        autor = response.xpath(xpath_autor).get(
        )

        # Dobivanje tekstova i spajanje u jedan string
        text_list = response.xpath(xpath_text).getall()
        text = ' '.join([str(text) for text in text_list])

        # Dobivanje broja komentara
        br_komentara = response.xpath(xpath_br_komentara).get()

        # Dobivanje tagova i spajanje u jedan string
        tagovi_list = response.xpath(xpath_tagovi).getall()
        tagovi = ' '.join([str(tag) for tag in tagovi_list])"""

        # Putanje za članke koji su uživo pisani
        #
        # xPath do kategorije
        xpath_kategorija = "//p[@class='category-name']/text()"

        # xPath do naslova
        xpath_naslov = "//span[@class='title-wrap-link']/h2[@class='main-news-title']/text()"

        # xPath do autora
        xpath_autor = "//div[@class='author']/div[@class='author-text']/div[@class='author-name']/a/text()"

        # xPath do datuma
        xpath_datum = "//div[@class='time-and-comment-wrapper']/time[@class='pub-date']/span[@class='date']/text()"

        # xPath do broja komentara
        xpath_br_komentara = "///span[@class='black-number']/text()"

        # xPath do uvoda
        xpath_uvod = "//h3[@class = 'story-lead']/text()"

        # xPath do texta
        xpath_text = "//div[@class='developing-story-segment']/p/text()"

        # xPath do tagova
        xpath_tagovi = "//div[@class='tags-content']/div[@class='tags']/a[@class='primary-tag']/text()"

        # Dobivanje naslova
        naslov = response.xpath(xpath_naslov).get()

        # Dobivanje uvodnog teksta
        uvod = response.xpath(xpath_uvod).get()

        # Dobivanje kategorije
        kategorija = response.xpath(xpath_kategorija).get()

        # Dobivnje datuma
        datum = response.xpath(xpath_datum).get(
        )

        # Dobivanje autora
        autor = response.xpath(xpath_autor).get(
        )

        # Dobivanje tekstova i spajanje u jedan string
        text_list = response.xpath(xpath_text).getall()
        text = ' '.join([str(text) for text in text_list])

        # Dobivanje broja komentara
        br_komentara = response.xpath(xpath_br_komentara).get()

        # Dobivanje tagova i spajanje u jedan string
        tagovi_list = response.xpath(xpath_tagovi).getall()
        tagovi = ' '.join([str(tag) for tag in tagovi_list])

        # Dobivanje linka
        url = response.request.url

        # Brisanje znaka ; (ponekad naslov ima dva dijela pa je dodan taj znak)
        try:
            naslov = naslov.replace(";", "")
        except:
            pass
        try:
            uvod = uvod.replace(";", "")
        except:
            pass
        try:
            kategorija = kategorija.replace(";", "")
        except:
            pass
        try:
            autor = autor.replace(";", "")
        except:
            pass
        try:
            text = text.replace(";", "")
        except:
            pass
        try:
            tagovi = tagovi.replace(";", "")
        except:
            pass
        try:
            datum = datum.replace(";", "")
        except:
            pass
        try:
            br_komentara = br_komentara.replace("(", "")
            br_komentara = br_komentara.replace(")", "")
        except:
            pass

        # Spremanje preuzetih podataka
        x = Vijest(naslov, uvod, kategorija, datum,
                   autor, text, br_komentara, tagovi, url)

        # Spremanje podatka u csv datoteku
        x.spremi_u_csv()

        # Ispis za testiranje
        # x.ispis_vijesti()

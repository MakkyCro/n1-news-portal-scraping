"""
    Ova datoteka vrši analizu nad preuzetim podacima. Prolazi se kroz sve članke i traže se članci vezani za koronavirus. Prebrojavaju se:
        - članci (ukupno, mjesečno i dnevno) koji su vezani s koronom
        - pojmovi koji se pojavljuju u člancima (grupirani mjesečno i ukupno)
        - kategorije koje spominju i ne spominju koronu
        - autori i datumi koji spominju i ne spominju koronu.
"""

import csv
import re
import numpy as np
import datetime
from pathlib import Path
from collections import Counter


# Funkcija za izradu liste koja sadrži samo nule
def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros


# Stvaranje prazne liste
keywords = []
stopwords = []
podaci = []

kategorije_s = []
kategorije_bez = []

rijeci = []
rijeci_mj = []
rijeci_dan = []

autori_s = []
datumi_s = []
datumi_bez = []
autori_bez = []

nadeni_dan = []
nadeni_mj = []
nadeni_uk = []

# Stvaranje varijabla za prebrojavanje
br_cl_s_poj_uk = 0
br_cl_bez_poj_uk = 0
br_cl_s_poj_mj = 0
br_cl_bez_poj_mj = 0
br_cl_s_poj_dan = 0
br_cl_bez_poj_dan = 0

dan = 1
mjesec = 1
dan_kraj = 30
mjesec_kraj = 11

header = True

# Izrađivanje direktorija u kojem se spremaju rezultati u obliku tekstualnih datoteka
try:
    Path("rezultati").mkdir(parents=True, exist_ok=True)
except:
    pass


# Otvaranje popisa zaustavnih rijeci
with open('zaustavne.txt', 'r') as f:
    stopwords = [line.replace("\n", "").replace("\r", "") for line in f]


# Otvaranje popisa keyworda
with open('keywords.txt', 'r') as f:
    keywords = [line.replace("\n", "").replace("\r", "") for line in f]

print(keywords)

# Otvaranje preuzetih podataka, prolazak kroz sve naslove, uvode, tekstove i tagove te spremanje u listu
with open("scrapanje_novosti/podaci_final.csv", "r", encoding='utf-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        if header:
            header = False
            continue
        else:
            # Učitavanje stupaca i || znakova po kojima ćemo splitati stringove
            podaci.append(lines[5].strip()+"||"+lines[2]+lines[6]+lines[7] +
                          "||"+lines[9]+"||"+lines[3].strip().lower()+"||"+lines[0]+"||"+lines[4].strip())

# Stvaranje liste s nulama u koju spremamo broj jedinstvenih i broj ponavljanja pojedinog keyworda u svim člancima
"""
clanci_key_s_uk = zerolistmaker(len(keywords))
clanci_key_s_mj = zerolistmaker(len(keywords))
clanci_key_s_dan = zerolistmaker(len(keywords))
"""

# Pomoćna binarna varijabla za prebrojavanje
naden = False


# Algoritam radi sve dok ne dođe do datuma za kraj
while (True):
    try:
        # Provjera postoji li navedeni datum (jednostavno rješenje za promijenu mjeseca, odnosno prijelazak na sljedeći mjesec)
        newDate = datetime.datetime(2020, mjesec, dan)
        print("\nDatum:", str(newDate).replace(" 00:00:00", ""))

        # Prolazak kroz sve podatke, provjera datuma i nakon toga provjera hotkeyeva
        for line in podaci:

            # print(line+"\n\n")
            # Dobivanje datuma članka
            datum = line.split("||")[0]
            # print(datum)
            # Dobivanje dana članka
            dan_clanak = int(datum.split(".")[0].lstrip("0"))
            # Dobivanje mjeseca članka
            mj_clanak = int(datum.split(".")[1].lstrip("0"))
            # Usporedba radi li se o dobrom datumu

            if dan_clanak == dan and mj_clanak == mjesec:
                # Dobivanje teksta nad kojim vršimo provjeru
                tekst = line.split("||")[1].lower()+line.split("||")[2].lower()
                kategorija = line.split("||")[3]
                id_clanka = line.split("||")[4]
                autor = line.split("||")[5]

                tekst = tekst.replace("\\p{Pd}", " ")
                tekst = tekst.replace("\u200b", " ")
                tekst = tekst.replace("[", " ")
                tekst = tekst.replace("]", " ")
                tekst = tekst.replace("-", " ")
                tekst = tekst.replace("\\", " ")
                tekst = tekst.replace("@", " ")
                tekst = tekst.replace("–", " ")

                tekst = re.sub(r'[.,]', ' ', tekst)

                tekst = re.sub(
                    r'[-\|\'‘`¸"″“„/!-$%#”;:&()?0-9]', ' ', tekst)

                tekst = re.sub(r'\s+', ' ', tekst)

                if "vili beroš" in tekst:
                    tekst = tekst.replace("vili beroš", "vili-beroš")
                if "alemka markotić" in tekst:
                    tekst = tekst.replace("alemka markotić", "alemka-markotić")
                if "stožera c" in tekst:
                    tekst = tekst.replace("stožera civilne", "stožera-civilne")
                if "stožeru c" in tekst:
                    tekst = tekst.replace("stožeru civilne", "stožeru-civilne")
                if "stožeri c" in tekst:
                    tekst = tekst.replace("stožeri civilne", "stožeri-civilne")
                if "stožer c" in tekst:
                    tekst = tekst.replace("stožer civilne", "stožer-civilne")
                if "johnson johnson" in tekst:
                    tekst = tekst.replace("johnson johnson", "johnson-johnson")
                if "moderna inc" in tekst:
                    tekst = tekst.replace("moderna inc", "moderna-inc")
                if "gilead sciences" in tekst:
                    tekst = tekst.replace("gilead sciences", "gilead-sciences")

                # Brisanje zaustavnih riječi

                for word in stopwords:
                    if " " + word + " " in tekst:
                        tekst = tekst.replace(" " + word + " ", " ")

                tekst_rijeci = tekst.split(" ")

                rijeci.extend(tekst_rijeci)
                rijeci_dan.extend(tekst_rijeci)
                rijeci_mj.extend(tekst_rijeci)

                # print(tekst)
                # Prolazak kroz sve definirane keywordove
                for keyword in keywords:
                    # Provjera postoji li trenutan hotkey u tekstu (naslov, uvod, tekst i tagovi) članka

                    # if keyword in tekst_rijeci:
                    for j, rijec in enumerate(tekst_rijeci):
                        if rijec == keyword:
                            if not naden:
                                # Dodavanje u sumu članaka koji sadrže barem jedan od pojmova
                                br_cl_s_poj_uk += 1
                                br_cl_s_poj_mj += 1
                                br_cl_s_poj_dan += 1
                                kategorije_s.append(kategorija)

                                autori_s.append(autor)
                                datumi_s.append(
                                    str(dan_clanak)+"."+str(mj_clanak))

                                naden = True

                            nadeni_dan.append(keyword)
                            nadeni_mj.append(keyword)
                            nadeni_uk.append(keyword)

                # Dodavanje u sumu članaka koji ne sadrže niti jedan od pojmova
                if not naden:
                    br_cl_bez_poj_uk += 1
                    br_cl_bez_poj_mj += 1
                    br_cl_bez_poj_dan += 1

                    kategorije_bez.append(kategorija)
                    autori_bez.append(autor)
                    datumi_bez.append(str(dan_clanak)+"."+str(mj_clanak))

            # Resetiranje pomoćne varijable
            naden = False

        if len(nadeni_dan) > 0:
            print(Counter(nadeni_dan).most_common())

        print(br_cl_s_poj_dan)
        print(br_cl_bez_poj_dan)

        with open('rezultati/analiza_dani.txt', 'a') as f:
            f.write(str(br_cl_s_poj_dan) + "\n")
            f.write(str(br_cl_bez_poj_dan) + "\n")

        # Resetiranje liste da pišu nule
        clanci_key_s_dan = zerolistmaker(len(keywords))
        br_cl_bez_poj_dan = 0
        br_cl_s_poj_dan = 0

        # Sljedeći dan
        dan += 1
        rijeci_dan = []
        nadeni_dan = []

    # Ukoliko ne postoji datum
    except ValueError:

        print(Counter(rijeci_mj).most_common(25), "\n")
        print(br_cl_s_poj_mj)
        print(br_cl_bez_poj_mj)
        print(Counter(nadeni_mj).most_common())

        with open('rezultati/analiza_mj.txt', 'a') as f:
            f.write(str(br_cl_s_poj_mj) + "\n")
            f.write(str(br_cl_bez_poj_mj) + "\n")

        with open('rezultati/mjesec_'+str(mjesec)+'.txt', 'w') as f:
            for k in Counter(rijeci_mj).most_common(25):
                k = re.sub(r"[()\']", "", str(k))
                f.write(k + "\n")

        with open('rezultati/nađeni_mj_'+str(mjesec)+'.txt', 'w') as f:
            for k in Counter(nadeni_mj).most_common():
                k = re.sub(r"[()\']", "", str(k))
                f.write(k + "\n")

        rijeci_mj = []
        nadeni_mj = []

        br_cl_s_poj_mj = 0
        br_cl_bez_poj_mj = 0

        # Resetiranje liste da pišu nule
        clanci_key_s_mj = zerolistmaker(len(keywords))

        mjesec += 1
        dan = 1

        # Uvijet za izlazak iz petlje
        if dan == 1 and mjesec == 12:
            break
        newDate = datetime.datetime(2020, mjesec, dan)

print(Counter(rijeci).most_common(25), "\n")
print(br_cl_s_poj_uk)
print(br_cl_bez_poj_uk)
print(Counter(nadeni_uk).most_common())

with open('rezultati/ukupno_sve_rijeci.txt', 'w') as f:
    for k in Counter(rijeci).most_common(25):
        k = re.sub(r"[()\']", "", str(k))
        f.write(k + "\n")

with open('rezultati/ukupno_pojmovi.txt', 'w') as f:
    for k in Counter(nadeni_uk).most_common():
        k = re.sub(r"[()\']", "", str(k))
        f.write(k + "\n")

with open('rezultati/analiza_uk.txt', 'w') as f:
    f.write(str(br_cl_s_poj_uk) + "\n")
    f.write(str(br_cl_bez_poj_uk))

with open('rezultati/analiza_datumi_s.txt', 'w') as f:
    for dat in Counter(datumi_s).most_common():
        k = re.sub(r"[()\']", "", str(dat))
        f.write(str(k) + "\n")


with open('rezultati/analiza_autori_s.txt', 'w') as f:
    for autor in Counter(autori_s).most_common():
        k = re.sub(r"[()\']", "", str(autor))
        f.write(str(k) + "\n")

with open('rezultati/analiza_datumi_bez.txt', 'w') as f:
    for dat in Counter(datumi_bez).most_common():
        k = re.sub(r"[()\']", "", str(dat))
        f.write(str(k) + "\n")

with open('rezultati/analiza_autori_bez.txt', 'w') as f:
    for autor in Counter(autori_bez).most_common():
        k = re.sub(r"[()\']", "", str(autor))
        f.write(str(k) + "\n")


with open('rezultati/analiza_kateg_s.txt', 'w') as f:
    for kat in Counter(kategorije_s).most_common():
        k = re.sub(r"[()\']", "", str(kat))
        f.write(str(k) + "\n")

with open('rezultati/analiza_kateg_bez.txt', 'w') as f:
    for kat in Counter(kategorije_bez).most_common():
        k = re.sub(r"[()\']", "", str(kat))
        f.write(str(k) + "\n")

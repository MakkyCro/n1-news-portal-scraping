"""
    Ova datoteka izrađuje grafičke prikaze prethodno dobivenih podataka, dobivenih nakon pokretanja python datoteke analiza.py
    Sadrži nekoliko predložaka za izradu različitih vrsta grafova (bar, treemap, pie...)
    Otvaramo podatke u rezultati folderu i vizualiziramo podatke
"""
import matplotlib
import matplotlib.pyplot as plt
import squarify
import re
from pathlib import Path
import numpy as np
import pandas as pd
import random


def random_color():
    r = random.random()
    b = random.random()
    g = random.random()
    return (r, g, b)


def crtajTreemap(vrijednosti, oznake, colors, spremi_u, naslov):

    plt.figure()
    squarify.plot(sizes=vrijednosti, label=oznake, color=colors, alpha=.5)
    plt.title(naslov)
    plt.axis('off')
    figure = plt.gcf()
    figure.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    plt.savefig(spremi_u)
    plt.show()

    plt.close()


def simplePlot(vrijednosti, oznake, spremi_u, naslov):

    fig, ax = plt.subplots()
    ax.plot(vrijednosti, oznake, color=random_color())

    ax.set(title=naslov)
    plt.axis([-10, 350, -10, 200])
    ax.grid()

    figure = plt.gcf()
    figure.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    plt.savefig(spremi_u)
    plt.show()

    plt.close()


def crtajBarGraf(vrijednosti, oznake, spremi_u, c, naslov):
    barWidth = 0.9

    plt.xticks([r + barWidth for r in range(len(oznake))],
               [line.split(", ")[0].strip() for line in oznake], rotation=90)

    bars = [line.split(", ")[0].strip() for line in oznake]
    y_pos = np.arange(len(bars))

    if c == "random":
        c = random_color()

    plt.bar(y_pos, vrijednosti, color=c, edgecolor='black')
    plt.title(naslov)

    plt.xticks(y_pos, bars, color='black')
    plt.yticks(color='black')

    figure = plt.gcf()
    figure.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    plt.savefig(spremi_u)
    plt.show()

    plt.close()


def heatMap(vrijednosti, oznake, mjeseci, spremi_u, c, naslov, cbar_kw={'orientation': 'vertical'}, cbarlabel="Broj članaka", **kwargs):

    fig, ax = plt.subplots()

    im = ax.imshow(vrijednosti, **kwargs)

    ax.set_xticks(np.arange(len(oznake)))
    ax.set_yticks(np.arange(len(mjeseci)))
    ax.set_xticklabels(oznake)
    ax.set_yticklabels(mjeseci)

    for i in range(len(mjeseci)):
        for j in range(len(oznake)):
            text = ax.text(j, i, vrijednosti_2d[i][j],
                           ha="center", va="center", color="w")

    ax.set_title(naslov)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    fig = plt.gcf()
    fig.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
    plt.savefig(spremi_u)
    plt.show()

    plt.close()


def donutPlot(vrijednosti, oznake, spremi_u, c, naslov):
    my_circle = plt.Circle((0, 0), 0.7, color='white')
    plt.pie(vrijednosti, labels=oznake, colors=c)
    p = plt.gcf()
    p.gca().add_artist(my_circle)
    plt.title(naslov)
    figure = plt.gcf()
    figure.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    plt.savefig(spremi_u)
    plt.show()

    plt.close()


def piePanda(vrijednosti, oznake, spremi_u, naslov):
    df = pd.DataFrame(vrijednosti, index=oznake, columns=[''])

    df.plot(kind='pie', subplots=True, figsize=(17, 9))

    plt.title(naslov)
    figure = plt.gcf()
    figure.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    plt.legend(loc='upper left')
    plt.savefig(spremi_u)
    plt.show()

    plt.close()


kat_s_oznake = []
kat_bez_oznake = []
kat_s_vrijednosti = []
kat_bez_vrijednosti = []
autori_bez_oznake = []
autori_bez_vrijednosti = []
autori_s_oznake = []
autori_s_vrijednosti = []
datumi_bez_oznake = []
datumi_bez_vrijednosti = []
datumi_s_oznake = []
datumi_s_vrijednosti = []

mj_s_bez_vrijednosti = []

oznake_keywords = []
oznake_combine = []
svi_s_uk = []
jed_s_uk = []
statistika = []


# Izrađivanje direktorija u kojem se spremaju rezultati u obliku tekstualnih datoteka
try:
    Path("grafovi").mkdir(parents=True, exist_ok=True)
except:
    pass

# Otvaranje podataka
with open('rezultati/analiza_uk.txt', 'r') as f:
    statistika = [int(re.sub(r"[A-ž :\n]", "", line)) for line in f]

with open('rezultati/analiza_kateg_s.txt', 'r') as f:
    kat_s_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_kateg_s.txt', 'r') as f:
    kat_s_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

with open('rezultati/analiza_kateg_bez.txt', 'r') as f:
    kat_bez_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_kateg_bez.txt', 'r') as f:
    kat_bez_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

with open('rezultati/analiza_mj.txt', 'r') as f:
    mj_s_bez_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "")) for line in f]

with open('rezultati/analiza_autori_bez.txt', 'r') as f:
    autori_bez_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_autori_bez.txt', 'r') as f:
    autori_bez_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

with open('rezultati/analiza_autori_s.txt', 'r') as f:
    autori_s_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_autori_s.txt', 'r') as f:
    autori_s_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

with open('rezultati/analiza_datumi_s.txt', 'r') as f:
    datumi_s_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_datumi_s.txt', 'r') as f:
    datumi_s_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

with open('rezultati/analiza_datumi_bez.txt', 'r') as f:
    datumi_bez_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_datumi_bez.txt', 'r') as f:
    datumi_bez_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

# for i in range(1, 12):
#     with open('rezultati/mjesec_'+str(i)+'.txt', 'r') as f:
#         oznake = [line.replace(", ", "\n") for line in f]

#     with open('rezultati/nađeni_mj_'+str(i)+'.txt', 'r') as f:
#         oznake_korona = [line.replace(", ", "\n") for line in f]

#     with open('rezultati/mjesec_'+str(i)+'.txt', 'r') as f:
#         vrijednosti = [int(line.replace("\n", "").replace(
#             "\r", "").split(", ")[1]) for line in f]

#     with open('rezultati/nađeni_mj_'+str(i)+'.txt', 'r') as f:
#         vrijednosti_korona = [int(line.replace("\n", "").replace(
#             "\r", "").split(", ")[1]) for line in f]

#     crtajBarGraf(vrijednosti, oznake, 'grafovi/mjesec_' +
#                  str(i)+'.png', 'random', 'Broj najčešćih riječi u člancima koji su vezani na koroni za '+str(i)+'.mjesec')

#     crtajBarGraf(vrijednosti_korona, oznake_korona, 'grafovi/pojmovi_korona_'+str(i)+'.png',
#                  'random', 'Broj najčešćih pojmova vezanih za koronavirus za ' + str(i) + '. mjesec')


with open('rezultati/analiza_dani.txt', 'r') as f:
    vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "")) for line in f]

vrijednosti_s_d = vrijednosti[::2]
vrijednosti_bez_d = vrijednosti[1::2]

clanci_po_danima = [sum(x) for x in zip(vrijednosti_s_d, vrijednosti_bez_d)]

simplePlot(np.arange(1, 336, 1), clanci_po_danima,
           'grafovi/broj_clanaka_s_po_danima.png', "Broj članaka po danima s i bez pojmova")

simplePlot(np.arange(1, 336, 1), vrijednosti_s_d,
           'grafovi/broj_clanaka_bez_po_danima.png', "Broj članaka po danima koji sadrže barem jedan pojam vezan za koronu")

simplePlot(np.arange(1, 336, 1), vrijednosti_bez_d,
           'grafovi/broj_clanaka_po_danima.png', "Broj članaka po danima koji ne sadrže niti jedan od traženih pojmova")


# Izrada glavnog grafa (prikaz odnosa među člancima s pronađenim i nepronađenim pojmovima)
bez_p_postotak = round(statistika[1]/36273 * 100, 2)
s_p_postotak = round(statistika[0]/36273 * 100, 2)
oznake_tmp = ["Članci bez pojmova\n"+str(bez_p_postotak)+"%",
              "Članci s barem jednim\npojmom "+str(s_p_postotak)+"%"]
vrijednosti_tmp = [statistika[1], statistika[0]]


# piePanda(vrijednosti, oznake_tmp, 'grafovi/statistika.png',
#          'Postotak članaka koji sadrže, odnosno ne sadrže tražene pojmove')


# oznake_tmp = kat_s_oznake
# vrijednosti_tmp = kat_s_vrijednosti
# colors = ["blue",
#           "green", "red", "cyan", "magenta", "yellow", "black", "white"]

# crtajTreemap(vrijednosti_tmp, oznake_tmp, colors, 'grafovi/kategorije_s.png',
#              'Treemap kategorija koji sadrže pojmove za koronu.')


# crtajBarGraf(vrijednosti_tmp, oznake_tmp, 'grafovi/kategorije_s_bar.png', 'blue',
#              'Broj članaka po kategoriji koji sadrže barem jedan od traženih pojmova (članci po kategorijama vezani za koronu)')


# oznake_tmp = kat_bez_oznake
# vrijednosti_tmp = kat_bez_vrijednosti
# colors = ["red",
#           "blue", "green", "cyan", "magenta", "yellow", "black", "white"]

# crtajTreemap(vrijednosti_tmp, oznake_tmp, colors, 'grafovi/kategorije_bez.png',
#              'Treemap kategorija koji ne sadrže pojmove za koronu.')


# crtajBarGraf(vrijednosti_tmp, oznake_tmp, 'grafovi/kategorije_bez_bar.png', 'yellow',
#              'Broj članaka po kategoriji koji ne sadrže ni jedan od traženih pojmova')


oznake_tmp = ["S barem jednim pojmom", "Bez pojmova"]
mjeseci = ["Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj",
           "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni"]
vrijednosti_2d = [mj_s_bez_vrijednosti[0:2], mj_s_bez_vrijednosti[2:4], mj_s_bez_vrijednosti[4:6], mj_s_bez_vrijednosti[6:8], mj_s_bez_vrijednosti[8:10],
                  mj_s_bez_vrijednosti[10:12], mj_s_bez_vrijednosti[12:14], mj_s_bez_vrijednosti[14:16], mj_s_bez_vrijednosti[16:18], mj_s_bez_vrijednosti[18:20], mj_s_bez_vrijednosti[20:22]]

heatMap(vrijednosti_2d, oznake_tmp, mjeseci,  'grafovi/heatmap_mjesecno.png', "black",
        'Heatmap - prikaz broja članaka koji sadrže i ne sadrže pojmove vezane za koronu (mjesečno)')


crtajBarGraf(autori_bez_vrijednosti[0:25], autori_bez_oznake[0:25], 'grafovi/autori_bez.png',
             'random', 'Broj članaka koji nisu vezani za koronavirus po datumu kroz cijeli period')

crtajBarGraf(autori_s_vrijednosti[0:25], autori_s_oznake[0:25], 'grafovi/autori_s.png',
             'random', 'Broj članaka koji su vezani za koronavirus po datumu kroz cijeli period')

crtajBarGraf(datumi_bez_vrijednosti[0:25], datumi_bez_oznake[0:25], 'grafovi/datumi_bez.png',
             'random', 'Broj članaka koji nisu vezani za koronavirus po datumu kroz cijeli period')

crtajBarGraf(datumi_s_vrijednosti[0:25], datumi_s_oznake[0:25], 'grafovi/datumi_s.png',
             'random', 'Broj članaka koji su vezani za koronavirus po datumu kroz cijeli period')

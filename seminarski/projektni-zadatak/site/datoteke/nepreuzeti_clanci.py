"""
    Ovaj program prolazi kroz preuzete podatke i provjerava koji članci nisu preuzeti te zapiše linkove u posebnu datoteku.
"""
import csv

# Stvaranje praznih lista
nepreuzeti_clanci = []
found = False


# Otvaranje preuzetih podataka i preuzimanje linkova bez podataka
with open("scrapanje_novosti/podaci.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        if lines[1] == '':
            nepreuzeti_clanci.append(lines[0])

# Zapisivanje praznih članaka za ponovno preuzimanje
with open("nepreuzeti_clanci.txt", "w") as f:
    for link in nepreuzeti_clanci:
        f.write("%s" % link+"\n")

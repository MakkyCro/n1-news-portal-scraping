"""
    U ovu klasu spremaju se sve vijesti, odnosno članci. Definirane su varijable naslov, uvod, kategorija, datum, autor, text, br_komentara, tagovi i poveznice.
"""

import csv


class Vijest(object):

    def __init__(self, naslov, uvod, kategorija, datum, autor, text, br_komentara, tagovi, url):
        self.naslov = naslov
        self.uvod = uvod
        self.kategorija = kategorija
        self.datum = datum
        self.autor = autor
        self.text = text
        self.br_komentara = br_komentara
        self.tagovi = tagovi
        self.url = url

    def ispis_vijesti(self):
        print(self.naslov, "\n", self.uvod, "\n",
              self.kategorija, "\n", self.datum, "\n", self.autor, "\n", self.text, "\n", self.br_komentara, "\n", self.tagovi, "\n", self.url, "\n\n")

    def spremi_u_csv(self):
        with open('podaci.csv', mode='a') as f:
            writer = csv.writer(f, delimiter=',', quotechar='"')
            writer.writerow([self.url, self.naslov, self.kategorija, self.autor,
                             self.datum, self.uvod, self.text, self.br_komentara, self.tagovi])

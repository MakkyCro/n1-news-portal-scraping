O PROJEKTU

Python datoteke na koje treba obratiti pozornost:
    - scrapanje_linkova.py (služi za preuzimanje svih linkova sa stranice)
    - nepreuzeti_linkovi.py (služi za provjeru koji linkovi su uspješno preuzeti)
    - nepreuzeti_clanci.py (služi za provjeru koji članci su prazni u preuzetom skupu)
    - brisanje_neispravnih.py (služi za finalno brisanje tih praznih članaka)
    - scrapanje_novosti/vijest.py (definira strukturu u koju se spremaju članci te metodu za spremanje u csv datoteku)
    - pretvaranje_json.py (pretvaranje preuzetih podataka u json oblik)
    - scrapanje_novosti/scrapanje_novosti/clanci_spider.py (služi za samo preuzimanje članaka i spremanje podataka u .csv datoteku)
                - za pokretanje treba promijeniti direktorij u terminalu u kojem se trenutno radi i upisati komandu - "scrapy crawl novosti"

Ostale datoteke:
    - scrapanje_novosti/linkovi.txt (sadrži sve linkove)
    - datum.txt (sadrži datum zadnje preuzetog linka - korišteno u slučaju ako se program pogubi)
    - linkovi_nepreuzeti.txt (sadrži popis linkova koji nisu preuzeti)
    - scrapanje_novosti/podaci.csv (preuzeti podaci sa neispravnim člancima i bez ID stupca)
    - scrapanje_novosti/podaci_final.csv (pročišćeni podaci s ID stupcom)
    - podaci_final.json (podaci u json obliku)
"""
    Ova datoteka sadrži algoritam za računanje Jaccardovog indexa po mjesecima. Rezultati se spremaju u odgovarajuću tekstualnu datoteku.
"""
import textdistance

jaccardovi_indexi = []

# Učitavanje svih mjeseca i računanje koeficijenta

for i in range(1, 11):
    with open('rezultati/mjesec_'+str(i)+'.txt', 'r') as f:
        a = [line.split(", ")[0] for line in f]
    with open('rezultati/mjesec_'+str(i+1)+'.txt', 'r') as f:
        b = [line.split(", ")[0] for line in f]

    s1 = set(a)
    s2 = set(b)

    jaccardovi_indexi.append(
        float(len(s1.intersection(s2)))/float(len(s1.union(s2))))
    print(float(len(s1.intersection(s2)))/float(len(s1.union(s2))))

    #suma = 0

    # Sumiranje jaccardovih indexa

    # for i, item in enumerate(a):
    #     suma += textdistance.jaccard(item, b[i])

    # Aritmetička srdina jaccardovog indexa
    # jaccardovi_indexi.append(suma/25)

# Spremanje koeficijenata
with open('rezultati/jaccard.txt', 'w') as f:
    for jac in jaccardovi_indexi:
        f.write(str(jac)+"\n")

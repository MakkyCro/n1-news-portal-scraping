"""
    Ovaj program prolazi kroz preuzete podatke i briše retke za koje nisu preuzeti podaci (neki linkovi ne rade)
"""
import csv

# Stvaranje praznih lista
neispravni_clanci = []
found = False

# Provjera postoji li naslov za članak. Ukoliko postoji sprema se u finalnu datoteku
with open('scrapanje_novosti/podaci.csv', 'r') as inp, open('scrapanje_novosti/podaci_final.csv', 'w') as out:
    writer = csv.writer(out)
    for row in csv.reader(inp):
        if row[1] != "":
            writer.writerow(row)

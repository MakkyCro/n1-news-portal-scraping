<h5 align="center">ODJEL ZA INFORMATIKU - AK. GOD.: 2020./21.</h5>
<h1 align="center">Projektni zadatak iz kolegija Upravljanja znanjem</h1>

<h5 align="center">Nositelj kolegija: izv. prof. dr. sc. Ana Meštrović</h5>
<h5 align="center">Asistent: dr. sc. Slobodan Beliga</h5>
<h5 align="center">Autor: Jasmin Makaj</h5>


## 1. Uvod
<p style="text-align:justify;">U ovom projektnom zadatku cilj je bio preuzeti novinske članke sa zadane platforme za emitiranje vijesti i aktualnosti te nakon toga analizirati te objave. Drugim riječima, cilj je napraviti vizualizaciju i interpretaciju podataka nad člancima koji su i nisu vezani za koronavirus. Dakle, potrebno je iz prikupljenih podataka programski generirati novo znanje različitim tehnikama. Na kraju je potrebno odrediti i diskurs kojim se služe autori ovog portala.</p>

## 2. Zadatak
<p style="text-align:justify;">Zadatak se sastojao od dva dijela. U prvom dijelu projekta potrebno je bilo preuzeti sve članke s odabranog portala i spremiti linkove te podatke članaka u odgovarajuće datoteke. Preuzete linkove spremili smo u običnu tekstualnu datoteku, dok smo podatke preuzete spremali u odgovarajućem formatu u .csv i .json datoteke. Drugi dio zadatka je zahtjevao da se napravi opširna analiza nad preuzetim podacima. Prikupljeni podaci se koriste u svrhu kolegija Upravljanja znanjem.</p>
Portal s kojega smo preuzimali podatke je [http://hr.n1info.com/](http://hr.n1info.com/). 

## 2.1. Prvi dio
### 2.1.1. Preuzimanje linkova
<p style="text-align:justify;">Prvi korak je bio izraditi programe za preuzimanje linkova svih članaka u periodu od 1.1.2020. do 30.11.2020. te nakon toga izraditi poseban program koji će posjetiti svaku poveznicu te preuzimati podatke. Za izradu scrapera linkova koristili smo Selenium paket. <a href="https://www.selenium.dev/">Selenium</a> je besplatan softver otvorenog koda koji služi kao driver koji automatizira preglednik. Primarno se koristi za testiranje web aplikacija. Za preuzimanje članaka koristimo zaseban program koji je brži od Seleniuma, a to je Scrapy. <a href="https://scrapy.org/">Scrapy</a> je framework otvorenog koda za preuzimanje podataka s željene web stranice.<br><br>Razlog zašto koristimo Selenium za preuzimanje linkova je taj što elementi stranice za navigaciju ne sadrže <i>href</i> atribute. Stoga odabiremo ovaj jednostavan i relativno spor način za preuzimanje linkova. Dodatni problemi s kojima smo se susreli ćemo u nastavku navesti.</p>

> Pritiskom na tipke 
++"CTRL"+"`"++ (tipka pored brojke jedan)
otvaramo ugrađeni terminal u VSCode alatu.

Za instalaciju selenium drivera i scrapy frameworka upisujemo sljedeće komande u terminal:

``` shell
pip install selenium
pip install scrapy
```

Kako bismo koristili driver za izradu prvog dijela projekta definiramo paket na početku projekta:
``` python
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import time
import datetime

firefox_profile = webdriver.FirefoxProfile()
firefox_profile.set_preference('permissions.default.stylesheet', 2)
firefox_profile.set_preference('permissions.default.image', 2)
firefox_profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')

driver = webdriver.Firefox(firefox_profile=firefox_profile)
driver.set_window_size(1200, 600)

driver.get("http://hr.n1info.com/Najnovije")
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">scrapanje_linkova.py</a></i>

<p align="justify">Izrađujemo varijablu <i>firefox_profile</i> koja definira da se slike i flashplayer ne učitava kako bi selenium brže radio i preuzimao linkove. U varijablu <i>driver</i> spremamo <i>webdriver</i> s profilom te definiramo veličinu prozora koja može biti problem kod pronalaženja elemenata. Drugim riječima, kako bi program mogao pristupati funkcijama elemenata, oni moraju biti u view portu preglednika. Zadnja linija koda, u prethodno navedenom bloku, pristupa stranici s koje preuzimamo linkove. Za dohvaćanje i navigiranje po elementima koristimo <i>xpath</i>. Primjer gumba za prihvaćanje cookiesa prilikom otvaranja web stranice nalazi se u nastavku. Definiramo xpath string koji dobijemo pregledom elemenata na web stranici (Inspect element). Nakon toga koristimo funkciju <i>find_element_by_xpath</i> kako bismo pronašli element u html datoteci. Dodajemo i funkciju za spavanje procesa kako bi se okvir za prihvaćanje kolačića zatvorio.</p>

``` python
xpath_cookies = "//a[contains(@class, 'CybotCookiebotDialogBodyButton')]"
driver.find_element_by_xpath(xpath_cookies).click()
time.sleep(1)
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">scrapanje_linkova.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na sliku kako biste ju vidjeli uvećanu
[![Kalendar](slike/kalendar.png)](slike/kalendar.png)

> Slika 1. N1 portal i otvoreni pregled elemenata.

<p align="justify">Na sličan način definiramo sve putanje do potrebnih elemenata poput gumba na kalendaru koji je prikazan na Slici 1. Vidimo da elementi za navigaciju kalendara nemaju <i>href</i> atribute. Kako bismo navigirali tim kalendarom, element <i>button</i> ima posebne atribute pomoću kojih se s javascriptom zahtjevaju članci od baze podataka. Mijenjanjem atributa <i>data-pika-month</i> i <i>data-pika-day</i> možemo navigirati na željeni datum bez da tražimo dodatne elemente poput strelice za mijenjanje mjeseca i dodatne provjere do kojeg mjeseca i dana smo dospjeli. Dakle, ukoliko atribute postavimo na <i>data-pika-month = "0"</i> i <i>data-pika-day = "1"</i> server vraća vijesti od 1.1., odnosno vidimo da mjeseci počinju od 0, a dani od 1. Nakon toga je potrebno seleniumom kliknuti na element. Dio koda koji opisuje navedeno mijenjanje atributa i pritisak na odabrani element je naveden u nastavku.</p>

``` python
dan = 1
mjesec = 1

driver.execute_script("arguments[0].setAttribute('data-pika-month', "+str(mjesec-1)+")", datum)
driver.execute_script("arguments[0].setAttribute('data-pika-day',"+str(dan)+")", datum)

datum.click()
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoeke">scrapanje_linkova.py</a></i>

<p align="justify">Navedeni kod stavljamo u while petlju kako bismo prolazili kroz sve dane i mjesece. While petlju prekidamo ukoliko dođemo do finalnog datuma, odnosno kada je dan jednak 30, a mjesec jednak 11. Prvi korak u while petlji je provjera postoji li navedeni datum. U try-except blok dodajemo provjeru u kojoj se svaka iteracija datuma provjerava. Ukoliko program, primjerice, dođe do datuma 32.1., try-except otkriva grešku definiranja datuma te povećava varijablu <i>mjesec</i> za jedan i postalja <i>dan</i> na 1. Dodatno, najčešće dnevno ima otprilike 100 članaka, taj broj varira iz dana u dan te je na većini potrebno na dnu pritisnuti element koji učitava dodatne članke tog dana. Slika 2. prikazuje primjer tog gumba.
U nastavku se nalazi blok koda koji je opisan.</p>

``` python
dan = 1
mjesec = 1
links = []
dan_kraj = 30
mjesec_kraj = 11

while not (dan == dan_kraj+1 and mjesec == mjesec_kraj): 
    try:
        newDate = datetime.datetime(2020, mjesec, dan)

        driver.find_element_by_xpath(xpath_gumb_kal).click()
        datum = driver.find_element_by_xpath(xpath_gumb_kal_odabir)
        driver.execute_script("arguments[0].setAttribute('data-pika-month', "+str(mjesec-1)+")", datum)
        driver.execute_script("arguments[0].setAttribute('data-pika-day',"+str(dan)+")", datum)

        datum.click()
        timeout_counter = 0

        while (True):
            try:
                time.sleep(1)
                element = WebDriverWait(driver, 1).until(
                    EC.element_to_be_clickable(
                        (By.XPATH, xpath_ucitaj_jos))
                )

                element.click()
            except:
                timeout_counter += 1
                time.sleep(.5)
                if timeout_counter == 3:
                    timeout_counter = 0
                    break

        driver.implicitly_wait(35)
        elems = driver.find_elements_by_xpath(xpath_linkovi)
        links = [elem.get_attribute('href') for elem in elems]

        with open('linkovi.txt', 'a') as f:
            for link in links:
                f.write("%s\n" % link)

        with open('datum.txt', 'w') as f:
            f.write(str(newDate))

        dan += 1

    except ValueError:
        mjesec += 1
        dan = 1
driver.quit()

```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">scrapanje_linkova.py</a></i>

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na sliku kako biste ju vidjeli uvećanu
[![Učitaj više](slike/ucitaj_vise.png)](slike/ucitaj_vise.png)

> Slika 2. N1 portal - gumb za učitavanje dodatnih vijesti

<p align="justify">Taj element se ne mora pojaviti, može se pojaviti jednom ili više puta. Najefikasniji način je u while petlji provjeravati postoji li taj gumb te ga pritisnuti uz dodatnu pauzu za učitavanje stranice. Ukoliko više ne postoji taj element, traži se <i>container</i> koji sadrži vijesti te se preuzimaju svi href atributi članaka. Nakon toga se ti linkovi spremaju u datoteke te se iteracije povećavaju dok ne dođe do uvjeta za prekid.<br>Linkovi su spremljeni u datoteku linkovi.txt te u nastavku možete vidjeti dio linkova.</p>

```
http://hr.n1info.com/Vijesti/a525231/Manjinci-ce-traziti-mjesto-potpredsjednika-Vlade-za-drustvene-djelatnosti.html
http://hr.n1info.com/Vijesti/a525217/O-Tomasic-Cacicu.-Kao-da-kupite-jedan-proizvod-a-na-adresu-dobijete-drugi.html
http://hr.n1info.com/Auto/a525235/TOP-FORD-LJETO-Jos-atraktivnija-Fordova-akcijska-trojka.html
http://hr.n1info.com/Vijesti/a525233/Odluke-Vlade-na-zatvorenom-dijelu-danasnje-sjednice.html
http://hr.n1info.com/Biznis/a525225/HT-podmiruje-tri-rate-Samsung-mobitela-u-novim-tarifama-bez-ugovorne-obveze.html
http://hr.n1info.com/Svijet/a525221/Koronavirus-se-rapidno-siri-gradom-gdje-je-Trump-nedavno-imao-predizborni-skup.html
http://hr.n1info.com/Vijesti/a525215/Tomasic-o-kolegi-koji-ce-u-Sabor-s-435-puta-manje-glasova-od-nje-Dajte-mu-sansu.html
http://hr.n1info.com/Vijesti/a525219/Grlic-Radman-odgovorio-na-prozivke-Vucica-Nasa-reakcija-ce-biti-adekvatna.html
http://hr.n1info.com/Vijesti/a525213/DIP-odbio-prigovor-Zeljka-Glasnovica-za-ponavljanje-izbora-u-inozemstvu.html
http://hr.n1info.com/Vijesti/a525211/Divjak-ostro-po-Borasu-Ne-mogu-70-godisnjaci-zaustavljati-mladje-i-sposobnije.html
http://hr.n1info.com/Regija/a525209/U-BiH-215-novih-slucajeva-zaraze-koronavirusom.html
```
> Blok teksta nalazi se u <i><a href="datoteke/#podaci-i-linkovi">linkovi.txt</a></i>

### 2.1.2. Struktura članaka
<p align="justify">Definiramo posebnu datoteku koja će predstavljati strukturu članka, odnosno vijesti. Dakle, radimo novu python datoteku u kojoj definiramo klasu Vijest. U konstruktor klase dodajemo sve vrijednosti koje ćemo prikupljati. Definiramo varijable za naslov, uvod, kategoriju, datum, autora, tekst, broj komentara, tagove i url. Napravili smo dodatnu pomoćnu funkciju za ispisivanje vijesti te funkciju za spremanje u csv datoteku. Za spremanje u .json format smo napravili posebnu datoteku koju ćemo spomenuti naknadno.</p>

``` python
import csv

class Vijest(object):

    def __init__(self, naslov, uvod, kategorija, datum, autor, text, br_komentara, tagovi, url):
        self.naslov = naslov
        self.uvod = uvod
        self.kategorija = kategorija
        self.datum = datum
        self.autor = autor
        self.text = text
        self.br_komentara = br_komentara
        self.tagovi = tagovi
        self.url = url

    def ispis_vijesti(self):
        print(self.naslov, "\n", self.uvod, "\n",
              self.kategorija, "\n", self.datum, "\n", self.autor, "\n", self.text, "\n", self.br_komentara, "\n", self.tagovi, "\n", self.url, "\n\n")

    def spremi_u_csv(self):
        with open('podaci.csv', mode='a') as f:
            writer = csv.writer(f, delimiter=',', quotechar='"')
            writer.writerow([self.url, self.naslov, self.kategorija, self.autor,
                             self.datum, self.uvod, self.text, self.br_komentara, self.tagovi])

```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">vijest.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

### 2.1.3. Preuzimanje članaka
<p align="justify">Nakon preuzimanja linkova slijedi i samo preuzimanje članaka. Za preuzimanje podataka izrađujemo scrapy projekt upisivanjem sljedeće komande u terminal:</p>

``` console
scrapy startproject scrapanje_novosti
```

<p align="justify">Automatski su generirane sve potrebne datoteke koje ćemo koristiti za scrapanje podataka s preuzetih linkova. U spiders mapu generiranog projekta dodajemo novu python datoteku koja će biti naš <i>spider</i> za preuzimanje podataka. Dodajemo pakete Spider i datoteku Vijest koja definira strukturu članaka. U nastavku definiramo klasu proizvoljnim imenom kojoj proslijeđujemo Spider objekt. Unutar klase definiramo varijablu <i>name</i> koje će se pisatu u terminal kod pokretanja <i>crawlera</i>. Kod koji preuzima članke nalazi se u nastavku.</p>

``` python
from scrapy.spiders import Spider
from vijest import Vijest

class NovostiSpider(Spider):
    name = 'novosti'
    with open("../nepreuzeti_clanci.txt") as f:
        urls = [url.strip() for url in f.readlines()]
        start_urls = urls


    def parse(self, response):
        xpath_kategorija = "//p[@class='category-name']/text()"
        xpath_naslov = "//span[@class='title-wrap-link']/h2[@class='main-news-title']/text()"

        kategorija = response.xpath(xpath_kategorija).get()
        naslov = response.xpath(xpath_naslov).get()
        try:
            kategorija = kategorija.replace(";", "")
        except:
            pass
        try:
            naslov = naslov.replace(";", "")
        except:
            pass

        x = Vijest(naslov, uvod, kategorija, datum,
                   autor, text, br_komentara, tagovi, url)

        x.spremi_u_csv()
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">clanci_spider.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Prvi korak je učitati sve linkove uz pomoć <i>open</i> funkcije te spremiti linkove u zasebnu varijablu. Funkcija <i>parse</i> je funkcija koja se automatski poziva prilikom pokretanja programa. Ponovno je potrebno definirati putanje do elemenata iz kojih ćemo preuzimati podatke. Koristimo <i>xpath</i> funkciju za pristupanje elementima te <i>get</i> funkciju za dobivanje teksta spremljenog u elementu.<br><br>Preuzete podatke spremamo u varijablu uz pomoć konstruktora u kojega proslijeđujemo vrijednosti. Vrijednosti također spremamo i u .csv datoteku pomoću <i>spremi_u_csv</i> funkcije.<br><br>Pokrećemo preuzimanje podataka slijedećom naredbom u terminalu:</p>

``` shell
scrapy crawl novosti
```

> Pritisnite na sliku kako biste vidjeli uvećanu sliku
[![Podaci](slike/podaci.png)](slike/podaci.png)

> Slika 3. Podaci spremljeni u .csv datoteku

### 2.1.4. Provjera preuzetih linkova i članaka
<p align="justify">Neke linkove i članke treba dodatno preuzeti, jer se nisu zapisali u datoteku. Scrapy brzo pristupa stranici, odnosno radi puno zahtjeva, pa server na kratko blokira pristupanje. Posljedica toga je nepreuzimanje svih članaka. Potrebno je napraviti dvije slične python datoteke koje provjeravaju koji od linkova su spremljeni u .csv datoteku, a koji nisu. Kasnije je cilj provjeriti koji članci su se preuzeli, a koji nisu te naknadno preuzeti one nepreuzete.</p>

``` python
import csv

linkovi_zavrseni = []
linkovi_svi = []
found = False

with open('../linkovi.txt', 'r') as f:
    linkovi_svi = f.readlines()

with open("./podaci.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        linkovi_zavrseni.append(lines[0])

counter = 1
maximum = len(linkovi_svi)
with open('../linkovi_nepreuzeti.txt', 'a') as f:
    for link in linkovi_svi:
        for l in linkovi_zavrseni:
            if link.strip() in l.strip():
                found = True
                break
            else:
                found = False
        if not found:
            f.write("%s" % link)
        else:
            found = False
        print(counter, "/", maximum)
        counter += 1
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">nepreuzeti_linkovi.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Potrebno je najprije učitati sve preuzete linkove, nakon toga i spremljene podatke u .csv datoteci s kojom ćemo uspoređivati linkove. Nakon toga slijedi jednostavan algoritam koji provjerava jedan po jedan link i zapisuje nepronađene u novu tekstualnu datoteku koju pokrećemo ponovno s programom za preuzimanje članaka.<br><br>Nakon što preuzmemo sve članke, provjeravamo koji od spremljenih članaka nisu imali pojedine vrijednosti spremljene. Detaljnijom provjerom smo mogli zaključiti da članci koji nisu imali spremljene naslove, tekstove, autore i slično imaju drugačiju strukturu na stranici. To su članci koji su se pratili uživo. Primjer: koronavirus u Americi, predsjednički izbori, parlamentarni izbori i slično. Potrebno je bilo spremiti u zasebnu datoteku linkove za koje nisu preuzete vrijednosti. Nakon toga želimo promijeniti kod u programu za preuzimanje članaka na način da definiramo posebne <i>xpathove</i> za navedene elemente. U nastavku se nalazi kod za provjeru članaka koji ne sadrže vrijednosti.</p>

``` python
import csv

nepreuzeti_clanci = []
found = False

with open("scrapanje_novosti/podaci.csv", "r") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        if lines[1] == '':
            nepreuzeti_clanci.append(lines[0])

with open("nepreuzeti_clanci.txt", "w") as f:
    for link in nepreuzeti_clanci:
        f.write("%s" % link+"\n")
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">nepreuzeti_članci.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Dakle, linkove koji nisu preuzeti ponovno preuzimamo na način da se prolazi kroz sve članke i provjerava se koji od članaka ne sadrže vrijednosti. Preuzimanje po potrebi treba više puta pokrenuti jer server može blokirati zahjeve.<br><br>Neki od članaka su obrisani te njih je potrebno izbaciti iz skupa podataka. Izrađujemo poseban mali dio koda koji briše poslijednjih par članaka koji su nepostojeći. U nastavku se nalazi dio koda za brisanje.</p>

``` python
import csv

neispravni_clanci = []
found = False

with open('scrapanje_novosti/podaci.csv', 'r') as inp, open('scrapanje_novosti/podaci_final.csv', 'w') as out:
    writer = csv.writer(out)
    for row in csv.reader(inp):
        if row[1] != "":
            writer.writerow(row)
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">brisanje_neispravnih.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Algoritam navedenog koda je jednostavan. Učitavamo datoteku s podacima te prolazimo jedan po jedan članak. Ukoliko članak sadrži tekst u naslovu dodajemo ga u novu datoteku koju zovemo podaci_final.csv. Program se ponavlja dok se ne provjere svi članci.</p>

### 2.1.5. Pretvaranje u JSON format

<p align="justify">Sljedeći korak koji nismo još napravili je spremanje skupa podataka u JSON formatu. U nastavku se nalazi kod za pretvaraje iz .csv datoteke u .json datoteku.</p>

``` python
import csv
import json

data = {}
data['podaci_json'] = []
header = True

with open("scrapanje_novosti/podaci_final.csv", "r", encoding='utf-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        if header:
            header = False
        else:
            data['podaci_json'].append(
                {'id': lines[0].strip(),
                'url': lines[1].strip(),
                'naslov': lines[2].strip(),
                'kategorija': lines[3].strip(),
                'autor': lines[4].strip(),
                'datum': lines[5].strip(),
                'uvod': lines[6].strip(),
                'text': lines[7].strip(),
                'br_komentara': lines[8].strip(),
                'tagovi': lines[9].strip()})

json_string = json.dumps(data, indent=4, ensure_ascii=False).encode('utf8')

with open("podaci_final.json", 'w', encoding='utf-8') as json_file:
    json_file.write(json_string.decode())

```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">pretvorba_json.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Učitavamo potrebne pakete (csv i json) za učitavanje i pisanje u datoteke. Čitamo podatke i spremamo u pripadnu varijablu. Funkcijom json.dump radimo enkodani string koji na kraju spremamo u .json datoteku.<br><br>Slijedi drugi dio projektnog zadatka koji je analiziranje dobivenih podataka te vizualizacija i interpretacija podataka.</p>

<br style="line-height:3px;display:block; margin: 3px 0;">

## 2.2. Drugi dio

<p align="justify">U ovom odlomku ćemo opisati datoteke kojima je analiza napravljena. Ukoliko želite vidjeti interpetirane podatke te vizualizaciju istih, pritisnite <a href="#23-interpretacija-i-vizualizacija-podataka">ovdje</a>. Drugi dio projektnog zadatka je bio izvršiti analizu nad preuzetim člancima. Tematika je aktualni virus COVID-19 koji je u poslijednjih godina dana vrlo česta vijest. Cilj je kvantificirati broj članaka koji su vezani za koronavirus. Student samostalno određuje pojmove poput koronavirusa, korone, covid-19 i slično kojima će provjeravati članke. Dodatno, trebamo napraviti analizu po danu, mjesecu i ukupnim objavama. Potrebno je i napraviti analizu po kategorijama članaka, odnosno provjeriti u kojim kategorijama se spominjala korona i koliko. Dodali smo i analizu datuma i autora, odnosno prikaz datuma i autora s najviše vezanih objava. Zadnji dio zadatka je analizirati najčešće korištene pojmove u objavama koje sadrže tražene pojmove te određivanje diskursa autora.</p>

### 2.2.1. Pomoćne datoteke

<p align="justify">Najprije ćemo definirati dvije pomoćne datoteke koje sadrže pojmove vezane za koronavirus i zaustavne riječi hrvatskoga jezika. U nastavku se nalazi dio popisa iz tih datoteka.</p>

```
korona
koronavirus
covid
zaraženi
maska
zaštitna-maska
dezinfekcija
dezinfekcijsko-sredstvo
vili-beroš
alemka-markotić
cjepivo
cijepljenje
respirator
```
> Skup svih pojmova nalazi se u <i><a href="datoteke/#ostale-datoteke">keywords.txt</a></i>. Blokovi teksta su skraćeni radi preglednosti i razumljivosti.

```
a
ako
ali
baš
bez
bi
bih
bila
bili
bilo
bio
bismo
bit
biti
bolje
bude
bumo
će
čega
ćemo
čemu
ćeš
često
```
> Skup svih zaustavnih riječi nalazi se u <i><a href="datoteke/#ostale-datoteke">zaustavne.txt</a></i>. Blokovi teksta su skraćeni radi preglednosti i razumljivosti.

### 2.2.2. Analiziranje članaka

<p align="justify">Radi jednostavnost i preglednosti opisati ćemo cijeli program u tri dijela: učitavanje paketa i postavljanje varijabli, analiza i spremanje rezultata u datoteke. Nakon definiranja pomoćnih datoteka, krećemo s izradom programa za analiziranje članaka. Definiramo novu python datoteku koja traži navedene pojmove u člancima i sprema podatke u zaseban folder. </p>

##### 2.2.2.1. Pripremanje paketa i varijabli

<p align="justify">Prije pisanja koda potrebno je ponovno u terminalu instalirati paket koji nam prebrojava listu. </p>

``` shell
pip install collections
```
<p>Prvi dio datoteke se sastoji od uključivanja pakate, definiranja praznih varijabli, izrade foldera i učitavanja tekstualnih datoteka s pojmovima i zaustavnim riječima.</p>

``` python
import csv
import re
import datetime
from pathlib import Path
from collections import Counter

keywords = []
stopwords = []
podaci = []

kategorije_s = []
kategorije_bez = []

rijeci = []
rijeci_mj = []
rijeci_dan = []

autori_s = []
datumi_s = []
datumi_bez = []
autori_bez = []

nadeni_dan = []
nadeni_mj = []
nadeni_uk = []

br_cl_s_poj_uk = 0
br_cl_bez_poj_uk = 0
br_cl_s_poj_mj = 0
br_cl_bez_poj_mj = 0
br_cl_s_poj_dan = 0
br_cl_bez_poj_dan = 0

dan = 1
mjesec = 1

header = True

try:
    Path("rezultati").mkdir(parents=True, exist_ok=True)
except:
    pass

with open('zaustavne.txt', 'r') as f:
    stopwords = [line.replace("\n", "").replace("\r", "") for line in f]

with open('keywords.txt', 'r') as f:
    keywords = [line.replace("\n", "").replace("\r", "") for line in f]

```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">analiza_dani_mj_uk.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Učitavamo potrebne pakete koje ćemo u nastavku objasniti. Paket <i>csv</i> nam je potreban za čitanje spremljenih članaka. Paket <i>re</i> predstavlja paket za regularne izraze, a njega koristimo za brisanje nepotrebnih znakova iz teksta. Kako bismo ponovno provjeravali datume koristimo <i>datetime</i> paket, a za izradu foldera u koji ćemo spremati podatke koristimo paket <i>Path</i>. Paket <i>Counter</i> nam je koristan za izradu <i>dataframe</i> koji će sadržavati prebrojene riječi i prebrojene pojmove. Definiramo prazne liste u koje će se spremati pojmovi, zaustavne riječi, riječi članka po danima, mjesecima i ukupno, autori i datumi vezani za koronu, autori i datumi nevezani za koronu i slično. Definiramo i varijable za prebrojavanje članaka koji su vezani za koronu te pomoćne varijable za provjeru datuma. Unutar <i>try-except</i> bloka izrađujemo mapu te nakon toga učitavamo pojmove i zaustavne riječi, uz pomoć <i>open</i> funkcije, iz tekstualnih datoteka i spremamo u pripadne liste.</p>

##### 2.2.2.2. Analiziranje članaka

<p align="justify">Nakon početnog dijela krećemo u samu analizu. U nastavku možemo vidjeti dio koda za analizu.</p>


``` python
with open("scrapanje_novosti/podaci_final.csv", "r", encoding='utf-8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for lines in csv_reader:
        if header:
            header = False
            continue
        else:
            podaci.append(lines[5].strip()+"||"+lines[2]+lines[6]+lines[7] +
                          "||"+lines[9]+"||"+lines[3].strip().lower()+"||"+lines[0]+"||"+lines[4].strip())

naden = False

while (True):
    try:
        newDate = datetime.datetime(2020, mjesec, dan)
        print("\nDatum:", str(newDate).replace(" 00:00:00", ""))
        for line in podaci:
            datum = line.split("||")[0]
            dan_clanak = int(datum.split(".")[0].lstrip("0"))
            mj_clanak = int(datum.split(".")[1].lstrip("0"))

            if dan_clanak == dan and mj_clanak == mjesec:
                tekst = line.split("||")[1].lower()+line.split("||")[2].lower()
                kategorija = line.split("||")[3]
                autor = line.split("||")[5]

                tekst = tekst.replace("\\p{Pd}", " ")
                tekst = tekst.replace("\u200b", " ")
                tekst = tekst.replace("[", " ")
                tekst = tekst.replace("]", " ")
                tekst = tekst.replace("-", " ")
                tekst = tekst.replace("\\", " ")
                tekst = tekst.replace("@", " ")
                tekst = tekst.replace("–", " ")

                tekst = re.sub(r'[.,]', ' ', tekst)

                tekst = re.sub(
                    r'[-\|\'‘`¸"″“„/!-$%#”;:&()?0-9]', ' ', tekst)

                tekst = re.sub(r'\s+', ' ', tekst)

                if "vili beroš" in tekst:
                    tekst = tekst.replace("vili beroš", "vili-beroš")
                if "alemka markotić" in tekst:
                    tekst = tekst.replace("alemka markotić", "alemka-markotić")
                if "stožera c" in tekst:
                    tekst = tekst.replace("stožera civilne", "stožera-civilne")
                if "stožeru c" in tekst:
                    tekst = tekst.replace("stožeru civilne", "stožeru-civilne")
                if "stožeri c" in tekst:
                    tekst = tekst.replace("stožeri civilne", "stožeri-civilne")
                if "stožer c" in tekst:
                    tekst = tekst.replace("stožer civilne", "stožer-civilne")
                if "johnson johnson" in tekst:
                    tekst = tekst.replace("johnson johnson", "johnson-johnson")
                if "moderna inc" in tekst:
                    tekst = tekst.replace("moderna inc", "moderna-inc")
                if "gilead sciences" in tekst:
                    tekst = tekst.replace("gilead sciences", "gilead-sciences")

                for word in stopwords:
                    if " " + word + " " in tekst:
                        tekst = tekst.replace(" " + word + " ", " ")

                tekst_rijeci = tekst.split(" ")

                for keyword in keywords:
                    for j, rijec in enumerate(tekst_rijeci):
                        if rijec == keyword:
                            if not naden:
                                br_cl_s_poj_uk += 1
                                br_cl_s_poj_mj += 1
                                br_cl_s_poj_dan += 1

                                rijeci.extend(tekst_rijeci)
                                rijeci_dan.extend(tekst_rijeci)
                                rijeci_mj.extend(tekst_rijeci)

                                kategorije_s.append(kategorija)

                                autori_s.append(autor)
                                datumi_s.append(str(dan_clanak)+"."+str(mj_clanak))

                                naden = True

                            nadeni_dan.append(keyword)
                            nadeni_mj.append(keyword)
                            nadeni_uk.append(keyword)

                if not naden:
                    br_cl_bez_poj_uk += 1
                    br_cl_bez_poj_mj += 1
                    br_cl_bez_poj_dan += 1

                    kategorije_bez.append(kategorija)
                    autori_bez.append(autor)
                    datumi_bez.append(str(dan_clanak)+"."+str(mj_clanak))

            naden = False

        if len(nadeni_dan) > 0:
            print(Counter(nadeni_dan).most_common())

        print(br_cl_s_poj_dan)
        print(br_cl_bez_poj_dan)

        with open('rezultati/analiza_dani.txt', 'a') as f:
            f.write(str(br_cl_s_poj_dan) + "\n")
            f.write(str(br_cl_bez_poj_dan) + "\n")

        br_cl_bez_poj_dan = 0
        br_cl_s_poj_dan = 0

        dan += 1
        rijeci_dan = []
        nadeni_dan = []
    except ValueError:

        print(Counter(rijeci_mj).most_common(25), "\n")
        print(br_cl_s_poj_mj)
        print(br_cl_bez_poj_mj)
        print(Counter(nadeni_mj).most_common())

        with open('rezultati/analiza_mj.txt', 'a') as f:
            f.write(str(br_cl_s_poj_mj) + "\n")
            f.write(str(br_cl_bez_poj_mj) + "\n")

        with open('rezultati/mjesec_'+str(mjesec)+'.txt', 'w') as f:
            for k in Counter(rijeci_mj).most_common(25):
                k = re.sub(r"[()\']", "", str(k))
                f.write(k + "\n")

        with open('rezultati/nađeni_mj_'+str(mjesec)+'.txt', 'w') as f:
            for k in Counter(nadeni_mj).most_common():
                k = re.sub(r"[()\']", "", str(k))
                f.write(k + "\n")

        rijeci_mj = []
        nadeni_mj = []

        br_cl_s_poj_mj = 0
        br_cl_bez_poj_mj = 0

        mjesec += 1
        dan = 1
        if dan == 1 and mjesec == 12:
            break
        newDate = datetime.datetime(2020, mjesec, dan)


```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">analiza_dani_mj_uk.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Najprije učitavamo .csv datoteku koja sadrži sve podatke te spremamo podatke u varijablu tako da uzimamo samo željene vrijednosti iz skupa. Nakon toga ulazimo u while petlju koja sadrži <i>try-catch</i> blok u kojem se definira datum s kojim se provjerava postojanje. Algoritam je zamišljen na način da program iterira datum te uzima sve članke s odabranim datumom. To radimo zbog toga što su neki članci preuzeti naknadno te se u skupu podataka nalaze i na kraju datoteke. Nakon definiranja datuma prolazimo kroz sve članke i provjeravamo odgovara li datum članka trenutnom datumu. Ukoliko se datumi podudaraju, uzima se sav potreban tekst za analizu, kategorija i autor. U varijablu <i>tekst</i> dodajemo naslov, uvodni tekst, tekst te tagove. Nakon toga potrebno je tekst dodatno počistiti od nepotrebnih znakova, a to su zagrade, zarezi, točke, upitnici i slično. Za čišćenje koristimo regularne izraze te <i>replace</i> funkciju. Dodatno, potrebno je neke riječi spojiti nakon brisanja svih nepotrebnih znakova. Razlog tome je taj da smo u datoteci u kojoj se nalaze pojmovi definirali vrijednosti koje sadrže razmak, a primjer tome je riječ vili-beroš. Poslije čišćenja i spajanja određenih riječi, potrebno je i obrisati tekst od zaustavnih riječi hrvatskoga jezika. Izrađujemo novu privremenu listu koju podijelimo po razmacima kako bismo mogli pronalaziti željene pojmove.<br><br>Algoritam za traženje pojmova je slijedeći. Prolazimo kroz sve pojmove navedene u datoteci uz pomoć <i>foreach</i> petlje. Unutar te petlje nalazi se još jedna slična petlja, s indeksiranim elementima, koja prolazi kroz sve riječi teksta. Ukoliko je pojam pronađen, imamo pomoćni <i>if</i> blok koja broji pronađene članke, sprema tekst u varijable za kvantificiranje riječi, autora i datuma. Također izvan <i>if</i> bloka nalazi se i lista za spremanje svih pronađenih pojmova koje na kraju kvantificiramo. Kad računalo završi prolazak kroz sve riječi, a nije pronađen niti jedan željeni pojam, povećavamo broj varijable nepronađenih članaka za 1 te dodajemo kategorije, autore i datume u odgovarajuće liste koje također prebrojavamo na kraju.<br><br>Završenim prolaskom kroz sve članke s određenim datumom spremamo podatke u odgovarajuću datoteku nazvanu <i>analiza_dani.txt</i>. U nju spremamo brojeve prebrojene članke, odnosno broj članaka koji sadrže ili ne sadrže tražene pojmove. Nakon toga resetiramo varijable te algoritam traži pojmove za novi dan. Ukoliko program dođe do nepostojećeg datuma, ispisuju se i spremaju rezultati za cijeli mjesec. Spremamo kvantificirane članke, najčešćih 25 riječi i sve pronađene pojmove po mjesecu u odgovarajuće datoteke te nakon toga ponovno resetiramo odgovarajuće varijable. Na dnu ovog bloka nalazi se uvjet za izlazak iz <i>while</i> petlje. </p>

##### 2.2.2.3. Spremanje ukupnih rezultata

<p align="justify">U nastavku se nalazi poslijednji dio koda navedenog programa za analiziranje.</p>


``` python

with open('rezultati/ukupno_sve_rijeci.txt', 'w') as f:
    for k in Counter(rijeci).most_common(25):
        k = re.sub(r"[()\']", "", str(k))
        f.write(k + "\n")

with open('rezultati/ukupno_pojmovi.txt', 'w') as f:
    for k in Counter(nadeni_uk).most_common():
        k = re.sub(r"[()\']", "", str(k))
        f.write(k + "\n")

with open('rezultati/analiza_uk.txt', 'w') as f:
    f.write(str(br_cl_s_poj_uk) + "\n")
    f.write(str(br_cl_bez_poj_uk))

with open('rezultati/analiza_datumi_s.txt', 'w') as f:
    for dat in Counter(datumi_s).most_common():
        k = re.sub(r"[()\']", "", str(dat))
        f.write(str(k) + "\n")


with open('rezultati/analiza_autori_s.txt', 'w') as f:
    for autor in Counter(autori_s).most_common():
        k = re.sub(r"[()\']", "", str(autor))
        f.write(str(k) + "\n")

with open('rezultati/analiza_datumi_bez.txt', 'w') as f:
    for dat in Counter(datumi_bez).most_common():
        k = re.sub(r"[()\']", "", str(dat))
        f.write(str(k) + "\n")

with open('rezultati/analiza_autori_bez.txt', 'w') as f:
    for autor in Counter(autori_bez).most_common():
        k = re.sub(r"[()\']", "", str(autor))
        f.write(str(k) + "\n")


with open('rezultati/analiza_kateg_s.txt', 'w') as f:
    for kat in Counter(kategorije_s).most_common():
        k = re.sub(r"[()\']", "", str(kat))
        f.write(str(k) + "\n")

with open('rezultati/analiza_kateg_bez.txt', 'w') as f:
    for kat in Counter(kategorije_bez).most_common():
        k = re.sub(r"[()\']", "", str(kat))
        f.write(str(k) + "\n")

```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">analiza_dani_mj_uk.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Poslijednji dio programa sprema ukupne rezultate u odgovarajuće tekstualne datoteke. Spremamo kvantificirane riječi svih članaka koji sadrže tražene pojmove te same prebrojene pojmove. Dodatno, spremamo i prebrojene članke te prebrojene kategorije, datume i autore koji sadrže i ne sadrže pojmove vezane za koronavirus.</p>

### 2.2.3. Jaccardov index

<p align="justify">Izrađujemo program za računanje Jaccardovog indeksa promijene diskursa po mjesecima temeljene na dobivenim najčešće korištenim riječima. Dakle, cilj nam je usporediti listu dobivenih riječi za odabrane mjesece. Prije nego prikažemo program, spomenimo što je Jaccardov index. Jaccardov indeks je koeficijent koji prikazuje sličnost između dva zadana skupa te se taj koeficijent naziva koeficijentom sličnosti. Ukoliko, primjerice, usporedimo dvije riječi "pas" i "pas", koeficijent će biti jednak 1.0. Ukoliko uspoređujemo riječi "korona" i "krevet" koeficijent će biti puno manji jer se riječi, točnije pojedini znakovi razlikuju. U nastavku se nalazi dio koda koji opisuje algoritam dobivanja koeficijenta.</p>


``` python

jaccardovi_indexi = []

for i in range(1, 11):
    with open('rezultati/mjesec_'+str(i)+'.txt', 'r') as f:
        a = [line.split(", ")[0] for line in f]
    with open('rezultati/mjesec_'+str(i+1)+'.txt', 'r') as f:
        b = [line.split(", ")[0] for line in f]

    s1 = set(a)
    s2 = set(b)

    jaccardovi_indexi.append(float(len(s1.intersection(s2)))/float(len(s1.union(s2))))
    print(float(len(s1.intersection(s2)))/float(len(s1.union(s2))))

with open('rezultati/jaccard.txt', 'w') as f:
    for jac in jaccardovi_indexi:
        f.write(str(jac)+"\n")
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">jaccardov_index.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Najprije izrađujemo praznu listu u koju ćemo spremiti vrijednosti. Nakon toga definiramo for petlju koja učitava vrijednosti za susjedne mjesece. Na temelju učitanih lista radi se jaccardov algoritam za dobivanje koeficijenta sličnosti koji na poslijetku spremamo u jaccard.txt datoteku.</p>

### 2.2.4. Vizualizacija podataka

<p align="justify">Spremljene rezultate je potrebno vizualizirati. Izrađujemo novu python datoteku za vizualiziranje podataka. Potrebno je preuzeti pakete za vizualiziranje, a to možemo napraviti upisivanjem naredbe u terminal.</p>

``` shell
pip install matplotlib
pip install pandas
pip install numpy
```

``` python
import matplotlib
import matplotlib.pyplot as plt
import squarify
import re
from pathlib import Path
import numpy as np
import pandas as pd
import random
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">scrapanje_linkova.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Potrebno je definirati razne vrste grafova koje ćemo koristiti. U funkcije za izradu grafova proslijeđujemo vrijednosti, oznake, putanju za spremanje, naslov grafa te po potrebi boju grafa. U nastavku se nalazi primjer jedne funkcije grafa.</p>

``` python
def heatMap(vrijednosti, oznake, mjeseci, spremi_u, c, naslov, cbar_kw={'orientation': 'vertical'}, cbarlabel="Broj članaka", **kwargs):

    fig, ax = plt.subplots()

    im = ax.imshow(vrijednosti, **kwargs)

    ax.set_xticks(np.arange(len(oznake)))
    ax.set_yticks(np.arange(len(mjeseci)))
    ax.set_xticklabels(oznake)
    ax.set_yticklabels(mjeseci)

    for i in range(len(mjeseci)):
        for j in range(len(oznake)):
            text = ax.text(j, i, vrijednosti_2d[i][j],
                           ha="center", va="center", color="w")

    ax.set_title(naslov)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    fig = plt.gcf()
    fig.set_size_inches(17, 9)
    manager = plt.get_current_fig_manager()
    manager.window.showMaximized()
    plt.tight_layout()
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")
    plt.savefig(spremi_u)
    plt.show()

    plt.close()
```
> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">vizualizacija.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Navedena funkcija izrađuje heatmap graf koji prikazuje frekvenciju članaka po mjesecima. Funkcija određuje što će se prikazivati na osima i kako. Definirana je i veličina grafa te postavljeno automatsko spremanje grafova. Definirali smo nekoliko funkcija za razne grafove.<br><br>U nastavku je primjer učitavanja podataka te crtanje grafa za kategorije.</p>

```python

kat_s_oznake = []
kat_bez_oznake = []
kat_s_vrijednosti = []
kat_bez_vrijednosti = []


with open('rezultati/analiza_kateg_s.txt', 'r') as f:
    kat_s_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_kateg_s.txt', 'r') as f:
    kat_s_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]

with open('rezultati/analiza_kateg_bez.txt', 'r') as f:
    kat_bez_oznake = [line.replace(", ", "\n") for line in f]

with open('rezultati/analiza_kateg_bez.txt', 'r') as f:
    kat_bez_vrijednosti = [int(line.replace("\n", "").replace(
        "\r", "").split(", ")[1]) for line in f]



oznake_tmp = kat_s_oznake
vrijednosti_tmp = kat_s_vrijednosti
colors = ["blue","green", "red", "cyan", "magenta", "yellow", "black", "white"]

crtajTreemap(vrijednosti_tmp, oznake_tmp, colors, 'grafovi/kategorije_s.png',
             'Treemap kategorija koji sadrže pojmove za koronu.')


crtajBarGraf(vrijednosti_tmp, oznake_tmp, 'grafovi/kategorije_s_bar.png', 'blue',
             'Broj članaka po kategoriji koji sadrže barem jedan od traženih pojmova (članci po kategorijama vezani za koronu)')


oznake_tmp = kat_bez_oznake
vrijednosti_tmp = kat_bez_vrijednosti
colors = ["red","blue", "green", "cyan", "magenta", "yellow", "black", "white"]

crtajTreemap(vrijednosti_tmp, oznake_tmp, colors, 'grafovi/kategorije_bez.png',
             'Treemap kategorija koji ne sadrže pojmove za koronu.')


crtajBarGraf(vrijednosti_tmp, oznake_tmp, 'grafovi/kategorije_bez_bar.png', 'yellow',
             'Broj članaka po kategoriji koji ne sadrže ni jedan od traženih pojmova')
```

> Blok koda nalazi se u <i><a href="datoteke/#python-datoteke">vizualizacija.py</a></i>. Blokovi koda su skraćeni radi preglednosti i razumljivosti.

<p align="justify">Definiramo prazne liste u koje, uz pomoć <i>open</i> funkcije, učitavamo vrijednosti i oznake koje želimo prikazati na grafovima. Za prikaz kategorija i brojnosti po kategorijama učitavamo odgovarajuće datoteke. Nakon učitavanja podataka definiramo dodatnu varijablu koja definira boje grafa. Pozivamo funkcije za crtanje <i>treemap</i> i stupičastog grafa. Isti postupak ponavljamo za kategorije koje nisu sadržavale pojmove. Na slične načine definiramo ostale grafove, učitavamo podatke te pozivamo funkcije za crtanje.<br><br>U slijedećem poglavlju vršimo interpretaciju dobivenih rezultata, odnosno grafova.</p>


## 2.3. Interpretacija i vizualizacija podataka

<p align="justify">U ovom odlomku ćemo interpretirati dobivene podatke i detaljnije analizirati generirane grafove. Polazni graf koji nas zanima je graf koji prikazuje odnos članaka koji sadrže i ne sadrže pojmove vezane za koronavirus.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/statistika.png)](slike/grafovi/statistika.png)

> Slika 4. Odnos članaka koji sadrže i ne sadrže pojmove vezane za koronavirus

&nbsp;|S barem jednim pojmom| bez pojmova
-----|-----|-----
Broj članaka| 18392| 17881

> Tablica 1. Odnos članaka s i bez pojmova vezanih za koronavirus

<p align="justify">Odmah na početku možemo vidjeti da je broj članaka vezanih za koronavirus dosta velik. Možemo pretpostaviti da se pojmovi pojavljuju u svakom drugom članku. Odnos prikazan po konkretnom broju članaka je <b>18392 (s barem jednim pojmom) : 17881 (bez pojmova)</b>. Možemo također pretpostaviti da je u tom broju vjerojatno i krivo pretpostavljenih članaka. Drugim riječima, ukoliko smo tražili riječi poput "maska", "cijepljenje", mogu se pronaći i članci karnevala ili nekog drugog cijepljenja nevezanog za koronavirus. Ali, vjerojatno je broj takvih objava relativno malen. Možemo ovaj graf prikazati na detaljniji način, odnosno izradom grafa koji je precizan i pokazuje promjene na dnevnoj bazi.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/broj_clanaka_po_danima.png)](slike/grafovi/broj_clanaka_po_danima.png)

> Slika 5. Broj članaka po danima od 1.1.2020. do 30.11.2020.

<p align="justify">Možemo vidjeti da broj objava po danu varira od 50, pa ćak do preko 175 objava dnevno. Možemo primijetiti i relativno ravan dio grafa u sredini. To se posljedica scrapera bez promijene agenata za pristupanje serveru. Server je blokirao zahtjeve te program nije mogao čitati zadnju stranicu ili zadnjih nekoliko stranica na zadanom danu.<br>Razlomimo ovaj graf na članke s i bez povezanosti s koronavirusom.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/broj_clanaka_bez_po_danima.png)](slike/grafovi/broj_clanaka_bez_po_danima.png)

> Slika 6. Broj članaka nevezanih za koronavirus po danima od 1.1.2020. do 30.11.2020.

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/broj_clanaka_s_po_danima.png)](slike/grafovi/broj_clanaka_s_po_danima.png)

> Slika 7. Broj članaka vezanih za koronavirus po danima od 1.1.2020. do 30.11.2020.



&nbsp;|1. sij 2020.|2. sij 2020.|3. sij 2020.|4. sij 2020.|5. sij 2020.|6. sij 2020.|7. sij 2020.|8. sij 2020.|9. sij 2020.|10. sij 2020.|11. sij 2020.|12. sij 2020.|13. sij 2020.|14. sij 2020.|15. sij 2020.|16. sij 2020.|17. sij 2020.|18. sij 2020.|19. sij 2020.|20. sij 2020.|21. sij 2020.|22. sij 2020.|23. sij 2020.|24. sij 2020.|25. sij 2020.|26. sij 2020.|27. sij 2020.|28. sij 2020.|29. sij 2020.|30. sij 2020.|31. sij 2020.|1. velj 2020.|2. velj 2020.|3. velj 2020.|4. velj 2020.|5. velj 2020.|6. velj 2020.|7. velj 2020.|8. velj 2020.|9. velj 2020.|10. velj 2020.|11. velj 2020.|12. velj 2020.|13. velj 2020.|14. velj 2020.|15. velj 2020.|16. velj 2020.|17. velj 2020.|18. velj 2020.|19. velj 2020.|20. velj 2020.|21. velj 2020.|22. velj 2020.|23. velj 2020.|24. velj 2020.|25. velj 2020.|26. velj 2020.|27. velj 2020.|28. velj 2020.|29. velj 2020.|1. ožu 2020.|2. ožu 2020.|3. ožu 2020.|4. ožu 2020.|5. ožu 2020.|6. ožu 2020.|7. ožu 2020.|8. ožu 2020.|9. ožu 2020.|10. ožu 2020.|11. ožu 2020.|12. ožu 2020.|13. ožu 2020.|14. ožu 2020.|15. ožu 2020.|16. ožu 2020.|17. ožu 2020.|18. ožu 2020.|19. ožu 2020.|20. ožu 2020.|21. ožu 2020.|22. ožu 2020.|23. ožu 2020.|24. ožu 2020.|25. ožu 2020.|26. ožu 2020.|27. ožu 2020.|28. ožu 2020.|29. ožu 2020.|30. ožu 2020.|31. ožu 2020.|1. tra 2020.|2. tra 2020.|3. tra 2020.|4. tra 2020.|5. tra 2020.|6. tra 2020.|7. tra 2020.|8. tra 2020.|9. tra 2020.|10. tra 2020.|11. tra 2020.|12. tra 2020.|13. tra 2020.|14. tra 2020.|15. tra 2020.|16. tra 2020.|17. tra 2020.|18. tra 2020.|19. tra 2020.|20. tra 2020.|21. tra 2020.|22. tra 2020.|23. tra 2020.|24. tra 2020.|25. tra 2020.|26. tra 2020.|27. tra 2020.|28. tra 2020.|29. tra 2020.|30. tra 2020.|1. svi 2020.|2. svi 2020.|3. svi 2020.|4. svi 2020.|5. svi 2020.|6. svi 2020.|7. svi 2020.|8. svi 2020.|9. svi 2020.|10. svi 2020.|11. svi 2020.|12. svi 2020.|13. svi 2020.|14. svi 2020.|15. svi 2020.|16. svi 2020.|17. svi 2020.|18. svi 2020.|19. svi 2020.|20. svi 2020.|21. svi 2020.|22. svi 2020.|23. svi 2020.|24. svi 2020.|25. svi 2020.|26. svi 2020.|27. svi 2020.|28. svi 2020.|29. svi 2020.|30. svi 2020.|31. svi 2020.|1. lip 2020.|2. lip 2020.|3. lip 2020.|4. lip 2020.|5. lip 2020.|6. lip 2020.|7. lip 2020.|8. lip 2020.|9. lip 2020.|10. lip 2020.|11. lip 2020.|12. lip 2020.|13. lip 2020.|14. lip 2020.|15. lip 2020.|16. lip 2020.|17. lip 2020.|18. lip 2020.|19. lip 2020.|20. lip 2020.|21. lip 2020.|22. lip 2020.|23. lip 2020.|24. lip 2020.|25. lip 2020.|26. lip 2020.|27. lip 2020.|28. lip 2020.|29. lip 2020.|30. lip 2020.|1. srp 2020.|2. srp 2020.|3. srp 2020.|4. srp 2020.|5. srp 2020.|6. srp 2020.|7. srp 2020.|8. srp 2020.|9. srp 2020.|10. srp 2020.|11. srp 2020.|12. srp 2020.|13. srp 2020.|14. srp 2020.|15. srp 2020.|16. srp 2020.|17. srp 2020.|18. srp 2020.|19. srp 2020.|20. srp 2020.|21. srp 2020.|22. srp 2020.|23. srp 2020.|24. srp 2020.|25. srp 2020.|26. srp 2020.|27. srp 2020.|28. srp 2020.|29. srp 2020.|30. srp 2020.|31. srp 2020.|1. kol 2020.|2. kol 2020.|3. kol 2020.|4. kol 2020.|5. kol 2020.|6. kol 2020.|7. kol 2020.|8. kol 2020.|9. kol 2020.|10. kol 2020.|11. kol 2020.|12. kol 2020.|13. kol 2020.|14. kol 2020.|15. kol 2020.|16. kol 2020.|17. kol 2020.|18. kol 2020.|19. kol 2020.|20. kol 2020.|21. kol 2020.|22. kol 2020.|23. kol 2020.|24. kol 2020.|25. kol 2020.|26. kol 2020.|27. kol 2020.|28. kol 2020.|29. kol 2020.|30. kol 2020.|31. kol 2020.|1. ruj 2020.|2. ruj 2020.|3. ruj 2020.|4. ruj 2020.|5. ruj 2020.|6. ruj 2020.|7. ruj 2020.|8. ruj 2020.|9. ruj 2020.|10. ruj 2020.|11. ruj 2020.|12. ruj 2020.|13. ruj 2020.|14. ruj 2020.|15. ruj 2020.|16. ruj 2020.|17. ruj 2020.|18. ruj 2020.|19. ruj 2020.|20. ruj 2020.|21. ruj 2020.|22. ruj 2020.|23. ruj 2020.|24. ruj 2020.|25. ruj 2020.|26. ruj 2020.|27. ruj 2020.|28. ruj 2020.|29. ruj 2020.|30. ruj 2020.|1. lis 2020.|2. lis 2020.|3. lis 2020.|4. lis 2020.|5. lis 2020.|6. lis 2020.|7. lis 2020.|8. lis 2020.|9. lis 2020.|10. lis 2020.|11. lis 2020.|12. lis 2020.|13. lis 2020.|14. lis 2020.|15. lis 2020.|16. lis 2020.|17. lis 2020.|18. lis 2020.|19. lis 2020.|20. lis 2020.|21. lis 2020.|22. lis 2020.|23. lis 2020.|24. lis 2020.|25. lis 2020.|26. lis 2020.|27. lis 2020.|28. lis 2020.|29. lis 2020.|30. lis 2020.|31. lis 2020.|1. stu 2020.|2. stu 2020.|3. stu 2020.|4. stu 2020.|5. stu 2020.|6. stu 2020.|7. stu 2020.|8. stu 2020.|9. stu 2020.|10. stu 2020.|11. stu 2020.|12. stu 2020.|13. stu 2020.|14. stu 2020.|15. stu 2020.|16. stu 2020.|17. stu 2020.|18. stu 2020.|19. stu 2020.|20. stu 2020.|21. stu 2020.|22. stu 2020.|23. stu 2020.|24. stu 2020.|25. stu 2020.|26. stu 2020.|27. stu 2020.|28. stu 2020.|29. stu 2020.|30. stu 2020.
-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----
S barem jednim pojmom|0|1|1|0|0|1|0|1|2|0|1|0|0|1|1|0|2|3|1|4|5|7|13|6|4|6|15|20|32|18|15|11|14|28|22|16|13|11|8|6|13|9|16|18|11|8|7|8|12|7|9|20|12|18|38|70|65|59|42|28|26|44|50|41|41|45|23|26|63|73|92|103|117|76|65|132|114|137|113|115|84|71|89|106|112|105|105|69|72|105|105|98|103|101|85|89|112|108|131|104|100|56|54|87|111|106|105|106|50|60|98|96|81|95|93|62|52|91|98|90|99|71|44|46|81|95|92|70|82|53|50|75|83|81|75|70|48|41|87|76|65|65|68|36|41|66|62|55|62|46|35|27|44|50|45|52|45|39|29|54|57|46|44|38|31|35|47|47|41|55|61|47|53|82|86|79|84|83|49|43|60|60|70|64|70|50|51|41|50|61|58|67|50|49|72|60|54|62|61|36|37|63|65|60|46|53|37|38|50|58|59|67|60|38|31|46|40|44|40|52|34|34|67|63|74|65|82|31|36|64|68|76|70|68|47|54|77|64|81|79|68|43|37|68|59|52|67|73|55|52|58|67|59|51|59|40|34|65|62|53|51|50|35|23|48|47|56|57|61|21|28|37|46|40|40|55|40|42|62|59|54|73|53|37|34|47|49|90|60|78|41|38|60|58|72|76|83|56|53|74|66|76|73|57|60|41|61|64|51|67|55|35|49|58|70|101|65|59|40|41|64|81|82|90|96|43|41|96|99|103|105|89|75|55|101
Bez pojmova|66|90|116|59|119|90|103|117|122|99|54|51|106|101|113|138|123|57|67|116|136|109|107|100|42|52|100|90|90|85|100|55|37|88|103|112|106|95|53|46|95|105|112|102|79|42|47|109|104|113|104|80|40|30|64|61|53|45|89|46|41|91|93|91|71|75|30|37|61|47|46|18|8|6|3|8|6|13|7|5|2|41|31|14|8|15|15|6|9|15|15|22|17|19|8|4|8|12|16|16|20|15|8|17|9|14|15|14|16|6|22|24|39|23|18|10|21|29|18|30|21|43|25|19|36|25|28|50|38|14|17|41|37|39|54|50|30|30|53|54|55|55|52|27|25|54|58|65|58|73|53|44|76|70|75|68|75|45|31|66|63|74|59|74|57|56|73|73|74|65|59|34|31|23|33|41|36|37|29|27|60|60|50|56|50|25|67|79|70|59|62|53|46|31|48|60|66|58|59|41|28|81|66|60|57|58|46|33|70|60|55|51|60|36|38|72|80|76|79|68|48|35|52|57|46|49|37|32|26|56|52|44|50|52|30|22|43|56|39|41|52|29|47|52|61|68|53|47|37|34|62|53|61|69|61|41|38|51|58|65|69|70|36|47|85|86|80|63|59|35|41|81|74|80|80|65|44|35|58|61|99|80|67|31|28|73|71|82|60|42|38|35|60|62|63|59|66|29|15|46|54|44|47|63|35|48|59|56|69|53|65|57|51|62|50|79|55|61|32|28|56|74|76|79|71|37|45|71|60|66|52|59|32|43|59

> Tablica 2. Brojnost članaka po danima


<p align="justify">Možemo primijetiti da se oko 75. dana, odnosno točnije 18.3.2020. se javlja anomalija u grafovima, odnosno dolazi do naglog povećanja broja članaka vezanih za koronavirus. Kako bismo utvrdili uzrok, potrebno je proučiti statistiku o koronavirusu. Pošto je koronavirus krenuo iz Kine i tek kasnije je zabilježen kod nas, potrebno je gledati cjelokupnu statistiku.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/18_3_pocetak.png)](slike/18_3_pocetak.png)

> Slika 8. Broj novozaraženih ljudi 18. ožujka 2020. godine. (izvor: <a href="https://www.google.com/search?q=coronavirus+data">JHU CSSE coronavirus data</a>)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/18_3_pocetak2.png)](slike/18_3_pocetak2.png)

> Slika 9. Broj smrtnih slučajeva u svijetu 18. ožujka 2020. godine. (izvor: <a href="https://www.google.com/search?q=coronavirus+data">JHU CSSE coronavirus data</a>)

<p align="justify">Iz grafova navedenim na slikama 8. i 9. jasno je vidljivo da je to trenutak u kojem je broj zaraženih i smrtnih slučajeva krenuo rast malo brže. Dokaz da se radi o datumu 18.3. možemo vidjeti na Slici 10.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/datumi_s.png)](slike/grafovi/datumi_s.png)

> Slika 10. Broj članaka vezanih za koronavirus sortirani po datumu

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/datumi_bez.png)](slike/grafovi/datumi_bez.png)

> Slika 11. Broj članaka nevezanih za koronavirus sortirani po datumu

<p align="justify">Prebrojili smo datume članaka koji sadrže pojam koronavirus te vidimo da 18.3. ima 137 članaka, 16.3. ima 132 članka vezana za koronavirus i tako redom. Na isti način ćemo prikazati članke koji ne sadrže niti jedan pojam vezan za koronavirus. Slika 11. prikazuje takav graf. Vidimo da se u drugom grafu pojavljuju datumi iz prvog i drugog mjeseca kada vijesti o COVID-19 virusu nisu bile aktualne. Možemo prikazati i podatke po mjesecima.<br><br>Slika 12. prikazuje heatmap graf koji prikazuje podatke na mjesečnoj bazi. Graf sadrži stupce "s barem jednim pojmom" i "bez pojmova" te retke "siječanj", "veljača", "ožujak", sve do "studenog".</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/heatmap_mjesecno.png)](slike/grafovi/heatmap_mjesecno.png)

> Slika 12. Kvantificirani članci sortirani po mjesecima

Mjesec| S barem jednim pojmom| Bez pojmova
-----|-----|-----
Siječanj| 160| 2918
Veljača| 599| 2215
Ožujak| 2519| 928
Travanj| 2721| 511
Svibanj| 1948| 1302
Lipanj| 1586| 1644
Srpanj| 1719| 1705
Kolovoz| 1741| 1496
Rujan| 1506| 1765
Listopad| 1816| 1692
Studeni| 2077| 1705

> Tablica 3. Odnos članaka s i bez pojmova vezanih za koronavirus grupiranih po mjesecima

<p align="justify">Prva dva mjeseca imaju relativno malo pojmova vezanih za koronavirus, dok su treći i četvrti, odnosno ožujak i travanj popunjeni takvim objavama. Krajem drugog mjeseca je koronavirus došao u naše susjedstvo te su zabilježeni prvi slučajevi kod i kod nas. Prvi zabilježeni slučaj je 20.2. te se početkom trećeg mjeseca krenuo širiti. Nakon četvrtog mjeseca, omjer se balansira vijestima parlamentarnih izbora koje će se održati u 5. mjesecu 2020. godine. U nastavku je omjer broja članaka vezanih i nevezanih za koronu postao gotovo podjednak.<br><br>U nastavku ćemo prikazati kvantificirane ukupne riječi i pojmovi po mjesecima. Iz tih grafova će biti vidljivo koje su najčešće korištene riječi te koji i koliko puta su se pojavili traženi pojmovi.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_1.png)](slike/grafovi/mjesec_1.png)

> Slika 13. Top 25 riječi za 1. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_2.png)](slike/grafovi/mjesec_2.png)

> Slika 14. Top 25 riječi za 2. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_3.png)](slike/grafovi/mjesec_3.png)

> Slika 15. Top 25 riječi za 3. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_4.png)](slike/grafovi/mjesec_4.png)

> Slika 16. Top 25 riječi za 4. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_5.png)](slike/grafovi/mjesec_5.png)

> Slika 17. Top 25 riječi za 5. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_6.png)](slike/grafovi/mjesec_6.png)

> Slika 18. Top 25 riječi za 6. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_7.png)](slike/grafovi/mjesec_7.png)

> Slika 19. Top 25 riječi za 7. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_8.png)](slike/grafovi/mjesec_8.png)

> Slika 20. Top 25 riječi za 8. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_9.png)](slike/grafovi/mjesec_9.png)

> Slika 21. Top 25 riječi za 9. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_10.png)](slike/grafovi/mjesec_10.png)

> Slika 22. Top 25 riječi za 10. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/mjesec_11.png)](slike/grafovi/mjesec_11.png)

> Slika 23. Top 25 riječi za 11. mjesec

<p align="justify">Grafovi od Slike 13. do Slike 25. nam prikazuju kvantificirane riječi članaka vezanih za koronavirus po mjesecima. Dakle, vidimo da se u prvom mjesecu vrlo malo puta spomenuo koronavirus te pojmovi vezani za njega, dok se u trećem mjesecu taj broj višestruko povećao. U grafu za prvi mjesec se pojavljuju riječi poput "Kina", "Wuhan", "virus" i slično. U slijedećih nekoliko mjeseci vidimo neke pojmove koje nismo zadali u tekstualnoj datoteci s pojmovima, a to su riječi poput "pandemija" i "epidemija".<br><br>Na isti način ćemo prikazati i najčešće pojmove koje smo definirali za traženje članaka vezanih za koronu.</p>


> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_1.png)](slike/grafovi/pojmovi_korona_1.png)

> Slika 24. Kvantificirani pronađeni pojmovi za 1. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_2.png)](slike/grafovi/pojmovi_korona_2.png)

> Slika 25. Kvantificirani pronađeni pojmovi za 2. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_3.png)](slike/grafovi/pojmovi_korona_3.png)

> Slika 26. Kvantificirani pronađeni pojmovi za 3. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_4.png)](slike/grafovi/pojmovi_korona_4.png)

> Slika 27. Kvantificirani pronađeni pojmovi za 4. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_5.png)](slike/grafovi/pojmovi_korona_5.png)

> Slika 28. Kvantificirani pronađeni pojmovi za 5. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_6.png)](slike/grafovi/pojmovi_korona_6.png)

> Slika 29. Kvantificirani pronađeni pojmovi za 6. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_7.png)](slike/grafovi/pojmovi_korona_7.png)

> Slika 30. Kvantificirani pronađeni pojmovi za 7. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_8.png)](slike/grafovi/pojmovi_korona_8.png)

> Slika 31. Kvantificirani pronađeni pojmovi za 8. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_9.png)](slike/grafovi/pojmovi_korona_9.png)

> Slika 32. Kvantificirani pronađeni pojmovi za 9. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_10.png)](slike/grafovi/pojmovi_korona_10.png)

> Slika 33. Kvantificirani pronađeni pojmovi za 10. mjesec

<p align="justify"></p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/pojmovi_korona_11.png)](slike/grafovi/pojmovi_korona_11.png)

> Slika 34. Kvantificirani pronađeni pojmovi za 11. mjesec

<p align="justify">Grafovi od Slike 25. do Slike 34. nam prikazuju kvantificirane pronađene pojmove po mjesecima. Dakle vidimo da se u prvom mjesecu vrlo malo puta spomenuo koronavirus te pojmovi vezani za njega, dok je u trećem mjesecu broj ponavljanja tih pojmova višestruko veći. Uzmimo, kao primjer, riječ "koronavirus" (ne uključuje: "koronavirusa", "koronavirusom"). U prvom mjesecu je ta riječ napisana 119 puta, dok je drugi mjesec riječ napisana 872 puta što je preko 7 puta više u odnosu na prethodni mjesec. Treći mjesec broj je riječ napisana 3654 puta. Broj ponavljanja te riječi se u dva mjeseca povećao za 30 puta. Četvrti mjesec broj pojavljivanja riječi se malo smanjuje, dok je u svibnju brojnost riječi pao na oko 1800 pojavljivanja. Slijedećih nekoliko mjeseci taj broj malo varira, dok se opet u studenom vraća na 2000 pojavljivanja.<br><br>Na poslijetku ćemo prikazati grafove na kojima je vidljivo u kojim kategorijama i koliko se spominju pojmovi vezani za koronavirus te ćemo još prikazati i koji autori su najčešće pisali o koronavirusu.</p>


> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/kategorije_bez.png)](slike/grafovi/kategorije_bez.png)

> Slika 35. Broj članaka po kategoriji kroz cijeli period koji nisu vezani za koronavirus (treemap)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/kategorije_s.png)](slike/grafovi/kategorije_s.png)

> Slika 36. Broj članaka po kategoriji kroz cijeli period koji su vezani za koronavirus (treemap)

<p align="justify">Treemap graf na Slici 35. prikazuje broj članaka grupiranih po kategorijama koji nisu vezani za koronavirus, dok je na Slici 36. prikazan treemap graf s člancima koji su vezani za pandemiju. Zanimljiviji nam je graf na kojem možemo primjetiti da se najčešće pisalo u kategorijama "vijesti", "svijet", "biznis", "regija" i slično, dok je kategorija "zdravlje" ironično imala 342 članka vezana za koronavirus. Ove grafove možemo prikazati i na stupičastim grafikonima koji se nalaze na Slici 37. i Slici 38.</p>


> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/kategorije_bez_bar.png)](slike/grafovi/kategorije_bez_bar.png)

> Slika 37. Broj članaka po kategoriji kroz cijeli period koji nisu vezani za koronavirus (stupičasti graf)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/kategorije_s_bar.png)](slike/grafovi/kategorije_s_bar.png)

> Slika 38. Broj članaka po kategoriji kroz cijeli period koji su vezani za koronavirus (stupičasti graf)

<p align="justify">Kao zadnje grafove ćemo prikazati autore koji su najčešće pisali o koronavirusu, odnosno prikazati ćemo grafove s brojnostima članaka koji su vezani i nisu vezani za epidemiju grupiranu po autorima.</p>

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/autori_bez.png)](slike/grafovi/autori_bez.png)

> Slika 39. Broj članaka po autoru kroz cijeli period koji nisu vezani za koronavirus

**Hina**|**N1 Hrvatska**|**N1 Info**|**PROMO**|**Bez autora**|**N1info**|**Tea Blažević**|**N1 BiH**|**N1 Beograd**|**Iva Puljić Šego**|**BETA**|**Sergej Županić**|**Matea Grgurinović**|**Elvir Mešanović**|**SRNA**|**Business Class**|**Anadolija**|**Ranko Stojanac**|**Hina Reuters**|**Matea Dominiković**|**Ivana Dragičević**|**Lejla Mujagić**|**FoNet**|**Zoran Pehar**|**Hari Kočić**|**Nova.rs**|**FENA**|**CNN**|**Katarina Brečić**|**Vanja Kranic**|**Selma Fukelj**|**Mia Peretić**|**Bright Side**|**BBC**|**Sport Klub**|**Domagoj Novokmet**|**Ivana Kopčić**|**N1**|**N1 Srbija**|**Adrijana Gavrić**|**SportKlub**|**World Dog Finder**|**Nikola Kojić**|**Jelena Zorić**|**Sandra Križanec**|**Anja Perković**|**BUSINESS CLASS / PROMO**|**Tanjug**|**Nataša Božić**|**Daily Mail**|**Goran Vrhovec**|**N1 Hrvataka**|**Maja Beker**|**N1 Sarajevo**|**Live Science**|**Tomaž Drozg**|**Bussines Class**|**Ilija Jandrić**|**Anka Bilić Keserović**|**Ika Ferrer Gotić**|**The Guardian**|**David Spaic-Kovacic**|**Medical News Today**|**VOA/Ivana Konstantinović**|**Ni Hrvatska**|**Nina Kljenak**|**bljesak.info**|**ni info**|**Beta/AP**|**Hrvoje Krešić**|**BIZNIS CLASS / PROMO**|**N1/Sun**|**Nenad Božović**|**Deutsche Welle**|**hiina**|**Beta/AFP**|**Tina Kosor Giljević**|**Jasmin Mušanović**|**Vijesti**|**Zvonko Komšić**|**Večernje novosti**|**Posao.hr**|**Healthline**|**UNICEF**|**Marko Perožić**|**Hina/AFP**|**Grad Knin**|**Barbara Matejčić**|**povrtnjak.com**|**Hrvatski Telekom**|**N1 Hrvatskla**|**Ana Mlinarić**|**Tea Blažević / Sergej Županić**|**N1 info / Hina**|**Gordana Ninković**|**Rade Županović**|**Mila Moralić**|**N1 Inf**|**Esmir Milavić**|**Nenad Milićević**|**Ivan Hrstić**|**Insider**|**Ana Sorić**|**Petface.net**|**Radio 021**|**muscleandfitnesshers**|**KoSSev**|**pinkvilla.com**|**Glas Amerike**|**haris mrkonja**|**Big Think**|**metropolitan.si**|**Zimnica.net**|**goodhousekeeping.com**|**Faruk Međedović**|**Ivan Nekić**|**24 sata.hr**|**The Times**|**Nikola Vučić**|**Glamour**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
10379|4054|2278|201|198|196|81|54|44|35|28|23|15|13|13|12|11|11|11|11|10|10|9|9|8|8|7|6|5|5|5|4|4|4|3|3|3|3|3|3|3|3|3|2|2|2|2|2|2|2|2|2|2|2|2|2|2|2|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1

> Tablica 4. Broj članaka nevezanih za koronavirus grupirani po autorima

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na graf kako biste vidjeli uvećanu sliku
[![](slike/grafovi/autori_s.png)](slike/grafovi/autori_s.png)

> Slika 40. Broj članaka po autoru kroz cijeli period koji su vezani za koronavirus

**Hina**|**N1 Hrvatska**|**N1 Info**|**Bez autora**|**N1 Beograd**|**N1info**|**PROMO**|**N1 BiH**|**Ivana Dragičević**|**Sergej Županić**|**Matea Grgurinović**|**BETA**|**Hina Reuters**|**Ranko Stojanac**|**Anadolija**|**FoNet**|**Matea Dominiković**|**Business Class**|**SRNA**|**FENA**|**Zoran Pehar**|**Nova.rs**|**Iva Puljić Šego**|**Ana Sorić**|**Hari Kočić**|**N1 info / Hina**|**hin**|**Hrvoje Krešić**|**Katarina Brečić**|**Ivana Kopčić**|**Rade Županović**|**Lejla Mujagić**|**Mia Peretić**|**Reuters**|**N1 Hrvatska/Hina**|**N1 Srbija**|**Jasmin Mušanović**|**Esmir Milavić**|**Elvir Mešanović**|**DW**|**RTS**|**Mašenka Vukadinović**|**BBC**|**CNN**|**Ivan Hrstić**|**Anja Perković**|**Domagoj Novokmet**|**Vanja Kranic**|**Beta/AFP**|**Nikola Kojić**|**N1 Hrvataka**|**Nataša Božić Šarić**|**BUSINESS CLASS / PROMO**|**Ivan Nekić**|**hna**|**telegraph.co.uk**|**Žaklina Tatalović**|**Stefan Stanković**|**Nataša Kovačev**|**Željko Raukar**|**Eva Mladiček**|**N1 Sarajevo**|**Media Servis/N1 Hrvatska**|**Dejan Farkaš**|**Beta /AP**|**Ivan Oreški/ Fininfo.hr**|**HEP**|**Ivana Salapura**|**BIRN**|**koronavirus**|**Maja Todić**|**Mia Peretić / Sergej Županić**|**Ni Hrvatska**|**n1 hrvaska**|**N1 Hrvatsla**|**Bussines Class**|**Marko Perožić**|**Tea Blažević**|**Deutsche Welle**|**Tina Kosor**|**Nataša Božić**|**Rasim Pavica**|**Healthline**|**N1**|**Radio Slobodna Evropa**|**theconversation.com**|**Faktor.ba**|**Suzana Stambol**|**Voice of America VOA**|**eatthis.com**|**The Guardian**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
11067|4821|1722|130|91|81|71|47|37|35|26|24|19|15|13|12|10|8|8|7|6|6|5|5|5|5|4|4|4|4|4|4|3|3|3|3|3|3|3|3|3|3|3|3|2|2|2|2|2|2|2|2|2|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1|1

> Tablica 5. Broj članaka vezanih za koronavirus grupirani po autorima

<p align="justify">Na ovom grafu ćemo se također fokusirati na graf s Slike 40. jer nam stupičasti graf sa Slike 39. prikazuje članke po autoru nevezane za koronavirus. Na Slici 40. vidimo da je autor "HINA", odnosno Hrvatska izvještajna novinska agencija, najčešći autor s 11067 članaka vezanih za koronavirus te su odmah iza te agencije i sam portal "N1 Hrvatska" s 4821 članka i "N1 info" s 1722 članka. Zanimljivo je vidjeti da 130 članka nemaju navedenog autora, dok su konkretno navedeni autori, kojima su vidljivi podaci poput imena i prezimena, napisali tek desetak članaka po osobi. Dakle, većina članaka nema konkretno navedenog autora.<br><br>Na Slici 41. se nalazi generirani wordcloud koji sadrži top 25 riječi kroz cijeli period, dok je na Slici 42. prikazan wordcloud s najčešćim pojmovima.</p>

> Pritisnite na sliku kako biste vidjeli uvećanu sliku
[![](slike/wordcloud_sve.png)](slike/wordcloud_sve.png)

> Slika 41. Wordcloud top 25 riječi pronađenih u člancima

Riječ| Brojnost
-----|-----
rekao| 19796
koronavirus| 19786
osoba| 16191
koronavirusa| 15362
ljudi| 13739
broj| 13497
mjere| 12026
mjera| 10049
slučajeva| 9453
koronavirusom| 9344
zaraze| 9276
covid| 8611
osobe| 8369
zaraženih| 7523
hrvatskoj| 6879
kazao| 6779
zaštite| 6492
bolesti| 6384
vlada| 6301
pandemije| 5982
tri| 5858
ukupno| 5815
covida| 5801
sata| 5744
epidemije| 5737

> Tablica 6. Popis top 25 riječi i njihove brojnosti kroz cijeli period

<br style="line-height:3px;display:block; margin: 3px 0;">

> Pritisnite na sliku kako biste vidjeli uvećanu sliku
[![](slike/wordcloud_pojmovi.png)](slike/wordcloud_pojmovi.png)

> Slika 42. Wordcloud najčešćih pojmova vezanih za koronavirus

Pojam| Brojnost
-----|-----
koronavirus| 19786
koronavirusa| 15362
koronavirusom| 9344
covid| 8611
zaraženih| 7523
covida| 5801
maske| 3203
cjepivo| 2656
stožera-civilne| 2156
stožer-civilne| 1895
korona| 1516
beroš| 1507
zaraženo| 1261
vili-beroš| 1073
masku| 1005
zaraženi| 956
zaražene| 858
zaražena| 796
markotić| 796
zaražen| 725
korone| 596
koronavirusu| 567
alemka-markotić| 432
cijepljenje| 412
pcr| 389
maska| 381
cijepljenja| 308
zaraženim| 230
koronu| 217
respirator| 209
dezinfekcija| 199
zaraženom| 193
pfizer| 184
koronom| 151
biontech| 147
stožeru-civilne| 141
astrazeneca| 127
maskom| 118
covidu| 113
sanofi| 61
koroni| 56
johnson-johnson| 38
curevac| 35
stožeri-civilne| 33
zaraženu| 29
gilead-sciences| 22
regeneron| 15
inovio| 14
moderna-inc| 10
glaxosmithkline| 7
novavax| 6
takeda| 2

> Tablica 7. Popis pojmova i brojnosti kroz cijeli period

<p align="justify">Možemo i naposljetku prikazati Jaccardov indeks promijene diskursa kojima su se autori koristili. Na Slici 43. nalazi se graf koji prikazuje vrijednosti u rasponu od 0.0 do 1.0 te one predstavljaju sličnosti u korištenim riječima. Vidljivo je da je u prvih dva mjeseca diskurs poprilično raznolik, a to možemo zaključiti malim koeficijentom. Kasnije se lista najčešćih riječi sve više podudara te se time dobiva visoki Jaccardov koeficijent. Drugim riječima, kasnije nema velike promijene u korištenim riječima.</p>

<div style="position: relative; width: 100%; height: 0; padding-top: 75.0000%;
 padding-bottom: 48px; box-shadow: 0 2px 8px 0 rgba(63,69,81,0.16); margin-top: 1.6em; margin-bottom: 0.9em; overflow: hidden;
 border-radius: 8px; will-change: transform;">
  <iframe style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; border: none; padding: 0;margin: 0;"
    src="https:&#x2F;&#x2F;www.canva.com&#x2F;design&#x2F;DAESL31xLn0&#x2F;view?embed">
  </iframe>
</div>
<a href="https:&#x2F;&#x2F;www.canva.com&#x2F;design&#x2F;DAESL31xLn0&#x2F;view?utm_content=DAESL31xLn0&amp;utm_campaign=designshare&amp;utm_medium=embeds&amp;utm_source=link" target="_blank" rel="noopener">Jaccard index</a> by Jasmin Makaj

> Slika 43. Jaccardov indeks promijene diskursa.

**Raspon**|**Jaccardov index**
:-----:|:-----:
Sij.-Velj.|0.47058823529411764
Velj.-Ožu.|0.5151515151515151
Ožu.-Tra.|0.7241379310344828
Tra.-Svi.|0.7241379310344828
Svi.-Lip.|0.6666666666666666
Lip.-Srp.|0.6666666666666666
Srp.-Kol.|0.7241379310344828
Kol.-Ruj.|0.9230769230769231
Ruj.-Lis.|0.9230769230769231
Lis.-Stu.|0.8518518518518519

> Tablica 8. Jaccardov indeks po mjesecima 

## Zaključak
<p align="justify">Uspješno su preuzeti linkovi i članci te dodatno napravljene pomoćne datoteke za uređivanje dobivenih podataka. Izrađene su i python datoteke za analiziranje i vizualizaciju dobivenih rezultata. Na kraju smo interpretirali podatke te prikazali najčešće korištene riječi i pojmove u člancima vezanim za koronavirus. Također, prikazali smo i brojnosti članaka vezanih za koronu po datumu, kategoriji i autoru podijeljenim po mjesecima. Prema najčešće korištenim riječima možemo zaključiti da se autori služe pomalo monotonim, negativnim i depresivnim diskursom. To možemo zaključiti prema riječima koje se ponavljaju, a to su primjerice "koronavirus", "broj", "osoba", "ljudi", "zaraza", "zaraženi", "slučajevi" i slično. Iz grafova je vidljivo da je oko pola objavljenih članaka bilo vezano ili je sadržavalo neke od pojmova vezanih za koronavirus. Iako je važno da se prenose ovakve informacije te da su ljudi dobro informirani, portali su iskoristili ovu priliku da bi dobili na pregledima članaka i na popularnosti. Poznata je činjenica da se šokantne i negativne vijesti bolje prenose.</p>

## Popis slika

Slika 1. N1 portal i otvoreni pregled elemenata.

Slika 2. N1 portal - gumb za učitavanje dodatnih vijesti

Slika 3. Podaci spremljeni u .csv datoteku

Slika 4. Odnos članaka koji sadrže i ne sadrže pojmove vezane za koronavirus

Slika 5. Broj članaka po danima od 1.1.2020. do 30.11.2020.

Slika 6. Broj članaka nevezanih za koronavirus po danima od 1.1.2020. do 30.11.2020.

Slika 7. Broj članaka vezanih za koronavirus po danima od 1.1.2020. do 30.11.2020.

Slika 8. Broj novozaraženih ljudi 18. ožujka 2020. godine. 

Slika 9. Broj smrtnih slučajeva u svijetu 18. ožujka 2020. godine. 

Slika 10. Broj članaka vezanih za koronavirus sortirani po datumu

Slika 11. Broj članaka nevezanih za koronavirus sortirani po datumu

Slika 12. Kvantificirani članci sortirani po mjesecima

Slika 13. Top 25 riječi za 1. mjesec

Slika 14. Top 25 riječi za 2. mjesec

Slika 15. Top 25 riječi za 3. mjesec

Slika 16. Top 25 riječi za 4. mjesec

Slika 17. Top 25 riječi za 5. mjesec

Slika 18. Top 25 riječi za 6. mjesec

Slika 19. Top 25 riječi za 7. mjesec

Slika 20. Top 25 riječi za 8. mjesec

Slika 21. Top 25 riječi za 9. mjesec

Slika 22. Top 25 riječi za 10. mjesec

Slika 23. Top 25 riječi za 11. mjesec

Slika 24. Kvantificirani pronađeni pojmovi za 1. mjesec

Slika 25. Kvantificirani pronađeni pojmovi za 2. mjesec

Slika 26. Kvantificirani pronađeni pojmovi za 3. mjesec

Slika 27. Kvantificirani pronađeni pojmovi za 4. mjesec

Slika 28. Kvantificirani pronađeni pojmovi za 5. mjesec

Slika 29. Kvantificirani pronađeni pojmovi za 6. mjesec

Slika 30. Kvantificirani pronađeni pojmovi za 7. mjesec

Slika 31. Kvantificirani pronađeni pojmovi za 8. mjesec

Slika 32. Kvantificirani pronađeni pojmovi za 9. mjesec

Slika 33. Kvantificirani pronađeni pojmovi za 10. mjesec

Slika 34. Kvantificirani pronađeni pojmovi za 11. mjesec

Slika 35. Broj članaka po kategoriji kroz cijeli period koji nisu vezani za koronavirus (treemap)

Slika 36. Broj članaka po kategoriji kroz cijeli period koji su vezani za koronavirus (treemap)

Slika 37. Broj članaka po kategoriji kroz cijeli period koji nisu vezani za koronavirus (stupičasti graf)

Slika 38. Broj članaka po kategoriji kroz cijeli period koji su vezani za koronavirus (stupičasti graf)

Slika 39. Broj članaka po autoru kroz cijeli period koji nisu vezani za koronavirus

Slika 40. Broj članaka po autoru kroz cijeli period koji su vezani za koronavirus

Slika 41. Wordcloud top 25 riječi pronađenih u člancima

Slika 42. Wordcloud najčešćih pojmova vezanih za koronavirus

Slika 43. Jaccardov indeks promijene diskursa.


## Popis tablica
Tablica 1. Odnos članaka s i bez pojmova vezanih za koronavirus

Tablica 2. Brojnost članaka po danima

Tablica 3. Odnos članaka s i bez pojmova vezanih za koronavirus grupiranih po mjesecima

Tablica 4. Broj članaka nevezanih za koronavirus grupirani po autorima

Tablica 5. Broj članaka vezanih za koronavirus grupirani po autorima

Tablica 6. Popis top 25 riječi i njihove brojnosti kroz cijeli period

Tablica 7. Popis pojmova i brojnosti kroz cijeli period

Tablica 8. Jaccardov indeks po mjesecima 


## Literatura

[1] - [Statistika o koroni](https://www.google.com/search?q=coronavirus+data)

[2] - [Informacije o N1 portalu](https://hr.n1info.com/o-nama/)

[3] - [Generator tablica za markdown](https://jakebathman.github.io/Markdown-Table-Generator/)

[4] - [Python kod na strackoverflow stranice](https://stackoverflow.com/)

[5] - [Wordcloud generator](https://www.wordclouds.com/)

[6] - [Jaccard index Wikipedia](https://en.wikipedia.org/wiki/Jaccard_index)

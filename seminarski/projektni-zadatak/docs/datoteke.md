## Python datoteke
> Datoteka za preuzimanje linkova (scrapanje_linkova.py)

> [Scrapanje linkova sa stranice N1 (selenium)](datoteke/scrapanje_linkova.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka koja definira strukturu članaka te sadrži funkcije za spremanje u .csv datoteku
> (vijest.py)

> [Python klasa članaka](datoteke/vijest.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za preuzimanje članaka (clanci_spider.py)

> [Spider za preuzimanje članaka (scrapy)](datoteke/clanci_spider.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za provjeru preuzetih linkova (nepreuzeti_linkovi.py)

> [Nepreuzeti linkovi](datoteke/nepreuzeti_linkovi.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za provjeru preuzetih članaka (nepreuzeti_clanci.py)

> [Nepreuzeti članci](datoteke/nepreuzeti_clanci.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za brisanje neispravnih članaka (brisanje_neispravnih.py)

> [Brisanje neispravnih članaka](datoteke/brisanje_neispravnih.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za pretvaranje preuzetih članaka u .json datoteku
> (pretvorba_json.py)

> [Pretvaranje .csv u .json](datoteke/pretvorba_json.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za analiziranje preuzetih članaka (analiza_dani_mj_uk.py)

> [Analiza preuzetih članaka](datoteke/analiza_dani_mj_uk.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za izradu grafova, odnosno vizualizacija podataka
> (vizualizacija.py)

> [Vizualizacija podataka](datoteke/vizualizacija.py)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka za računanje jaccardovih koeficijenata
> (jaccardov_index.py)

> [Jaccardovi indeksi](datoteke/jaccardov_index.py)
## Podaci i linkovi

> Datoteka koja sadrži sve članke spremljene u csv formatu
> (podaci_final.csv)

> [Članci - CSV datoteka](datoteke/podaci_final.csv)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Datoteka koja sadrži sve članke spremljene u json formatu
> (podaci_final.json)

> [Članci - JSON datoteka](datoteke/podaci_final.json)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Datoteka koja sadrži sve linkove (linkovi_final.txt)

> [Popis linkova](datoteke/linkovi_final.txt)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Datoteka koja sadrži linkove koji se nisu preuzeli (linkovi_nepreuzeti.txt)

> [Popis nepreuzetih linkovi](datoteke/linkovi_nepreuzeti.txt)

## Ostale datoteke

> Datoteka koja sadrži pojmove vezane za koronavirus (keywords.txt)

> [Keywords](datoteke/keywords.txt)

<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka koja sadrži zaustavne riječi koje izbacujemo (zaustavne.txt)

> [Zaustavne riječi](datoteke/zaustavne.txt)


<br style="line-height:3px;display:block; margin: 3px 0;">
> Datoteka koja sadrži opis datoteka (O projektu.txt)


> [O projektu](datoteke/O projektu.txt)

<br style="line-height:3px;display:block; margin: 3px 0;">

> Datoteka koja sadrži listu jaccardovih indeksa

> [Jaccardovi indeksi](datoteke/jaccard.txt)
